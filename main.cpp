#include <iostream>
#include <errno.h>
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
#include <libgen.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>
#include <limits.h>
#include <sys/types.h>
#include <sys/time.h>
#include "TernixTerminal.h"
#include <glib.h>
#include <sys/poll.h>
#include <boost/filesystem.hpp>
#include <c++/array>
#include <c++/thread>

#include "HexParser.h"

using namespace std;

#define TERNIX_USER_CMD_NULL							0x00
#define TERNX_USER_CMD_SHOW_ALL_INPUTS                  0x01
#define TERNX_USER_CMD_SHOW_ALL_OUTPUTS                 0x02
#define TERNIX_USER_CMD_SHOW_CONNECTED_ANALOG_MODULES   0x03
#define TERNIX_USER_CMD_SHOW_CONNECTED_PROPO_MODULES    0x04
#define TERNIX_USER_CMD_SHOW_INPUT_MODE                 0x05
#define TERNIX_USER_CMD_SET_DIGITAL_OUTPUT              0x06
#define TERNIX_USER_CMD_READ_DIGITAL_INPUT              0x07
#define TERNIX_USER_CMD_SETUP_INPUT_MODE                0x08
#define TERNIX_USER_CMD_SET_PROPO_OUTPUT_CHANNEL_DC     0x09
#define TERNIX_USER_CMD_SET_PROPO_OUTPUT_CHANNEL_FREQ   0x0a
#define TERNIX_USER_CMD_BOOTLOADER                      0x0b
#define TERNIX_USRE_CMD_READ_ANALOG_INPUT				0x0c

const gchar* user_guid_str =
        "\nTerNix ProtoType Tester:\n"
        "*******************************************\n"
        "* 1.  Show all input values.\n"
        "* 2.  Show all outputs.\n"
        "* 3.  Show connected analog modules.\n"
        "* 4.  Show connected propo modules.\n"
        "* 5.  Show all inputs mode(Digital/Analog).\n"
        "* 6.  Set digital output.\n"
        "* 7.  Read digital input.\n"
        "* 8.  Setup input mode(Digital/Analog).\n"
        "* 9.  Setup Propo output PWM DC.\n"
        "* 10. Setup Propo output PWM Freq.\n"
        "* 11. Bootloader.\n"
		"* 12. Read Analog input channel.\n"
        "*******************************************\n"
        "Please select one option from 1 to 12 to start...\n";

TernixTerminal * ternix = NULL;

gpointer TernixUserInterfaceIdleFunc(gpointer data);
gboolean isUserGiveCommandBeforeTimeOut(gchar * pdt, int wait_time);
gboolean captureUserInputWithTimeout(const gchar * msg, gchar * pdata, int wait_time);
gint TernixConvertUserInputToUInt(const gchar* instr, int wait_time = 5);
bool TernixHelpUserFindHexFile(const boost::filesystem::path & dir_path, // in this directory,
							   const std::string & file_name, // search for this name,
							   boost::filesystem::path & path_found); // placing path here if found

gboolean captureUserInputWithTimeout(const gchar * msg,  gchar * pdata, int wait_time)
{
	if(msg != NULL && msg[0] != '\0')
		cerr << msg << endl;

	if(!isUserGiveCommandBeforeTimeOut(pdata, wait_time)) {
		cerr << "Timeout, no input found!" << endl;
		return FALSE;
	}

	return TRUE;
}

gint TernixConvertUserInputToUInt(const gchar* instr, int wait_time)
{
    gchar user_input[64];
    guint user_input_int = 0;

    if(!captureUserInputWithTimeout(instr, user_input, wait_time))
    	return -1;

    if(string(user_input).compare("0") == 0)
		return 0;

    if((user_input_int = strtoull(user_input, (char**)NULL, 10)))
        return (gint)user_input_int;
    else {
        cerr << "Invalid input, try again" << endl;
        return -1;
    }
}

bool TernixHelpUserFindHexFile(const boost::filesystem::path & dir_path, // in this directory,
                const std::string & file_name, // search for this name,
                boost::filesystem::path & path_found ) // placing path here if found
{
  if (!exists(dir_path))
	  return false;

  boost::filesystem::directory_iterator end_itr; // default construction yields past-the-end

  for(boost::filesystem::directory_iterator itr(dir_path); itr != end_itr; ++itr) {
    if(boost::filesystem::is_directory(itr->status())) {
      if(TernixHelpUserFindHexFile(itr->path(), file_name, path_found))
    	  return true;
    }
    else if(itr->path().leaf() == file_name) {
    	path_found = itr->path();
		return true;
	}
  }
  return false;
}

int main()
{
    // New main loop
    GMainLoop* GpioMainLoop = g_main_loop_new(0, 0);
    // Initialize output and input objects
    // detect connected analog modules and proportional modules
    // create event handler corresponding to the connected modules
    // Initialize modules by sending configuration I2C message
    ternix = new TernixTerminal();

    // Monitoring user input CMD and execute right ones
    g_thread_new("User Interface thread", TernixUserInterfaceIdleFunc, (gpointer)ternix);

    g_main_loop_run(GpioMainLoop);

    return 0;
}

gboolean isUserGiveCommandBeforeTimeOut(gchar * pdt, int wait_time)
{
	struct pollfd input;

	input.fd = 0;
	input.events = POLLIN;

	if(poll(&input, 1, (wait_time * 1000)) == 0) {
		std::cin.clear();
		return FALSE;
	}

	cin.getline(pdt, 64);

	return TRUE;
}

gpointer TernixUserInterfaceIdleFunc(gpointer data)
{
    gint user_select = 0, temp = 0;
    gchar in_buff[64];
    guchar data_buff[16];
    string hex_file_name;
    boost::filesystem::path dir_path, found_path;
    TernixI2CBootLoader * bootloader = NULL;
    HexParser * parser = NULL;

    TernixOutput *out = NULL;
    TernixInput *in = NULL;

    TernixTerminal * ternix = (TernixTerminal * )data;
    TernixAddonModule * addonModule = NULL;

    while(TRUE) {
    	g_thread_yield();
    	if(isUserGiveCommandBeforeTimeOut(in_buff, 1) == FALSE)
    		continue;

    	if((user_select = TernixConvertUserInputToUInt(user_guid_str)) < 0)
    		user_select = TERNIX_USER_CMD_NULL;

		switch(user_select) {
			case TERNX_USER_CMD_SHOW_ALL_INPUTS:
				TernixInput::printOutAllDigitInputs();
				break;
			case TERNX_USER_CMD_SHOW_ALL_OUTPUTS:
				TernixOutput::printOutAllDigitOutputs();
				break;
			case TERNIX_USER_CMD_SHOW_CONNECTED_ANALOG_MODULES:
				ternix->printOutAllAnalogModulesConnectStatus();
				break;
			case TERNIX_USER_CMD_SHOW_CONNECTED_PROPO_MODULES:
				ternix->printOutAllPropoModulesConnectStatus();
				break;
			case TERNIX_USER_CMD_SHOW_INPUT_MODE:
				TernixInput::printOutAllInputsMode();
				break;
			case TERNIX_USER_CMD_SETUP_INPUT_MODE:
				break;
			case TERNIX_USER_CMD_READ_DIGITAL_INPUT:
				if((user_select = TernixConvertUserInputToUInt("Input ID:")) < 0)
					break;
				if((in = TernixInput::getTernixInput(user_select)) == NULL) {
					cerr << "No input was found by the input ID: " << (int)user_select << endl;
					break;
				}
				cerr << "Input: " << (int)in->getId() << " Digit: " << (int)in->getDigit() << endl;
				break;
			case TERNIX_USER_CMD_SET_DIGITAL_OUTPUT:
				if((user_select = TernixConvertUserInputToUInt("Output ID:")) < 0)
					break;
				if((out = TernixOutput::getTernixOutput(user_select)) == NULL) {
					cerr << "No output was found by the output ID: " << (int)user_select << endl;
					break;
				}
				cerr << "Output: " << (int)out->getId() << " GPIO: " << (int)out->getGpio() << " Value: " << (int)out->getDigit() << endl;
				if((user_select = TernixConvertUserInputToUInt("Change Output value to:")) < 0)
					break;

				if(user_select == OFF) out->setDigit(OFF);
				else if(user_select == ON) out->setDigit(ON);
				else {
					cerr << "Set output value failed, Please Provide a valid value(0/1)." << endl;
					break;
				}
				cerr << "Output: " << (int)out->getId() << " Value: " << (int)out->getDigit() << endl;
				break;
			case TERNIX_USER_CMD_SET_PROPO_OUTPUT_CHANNEL_DC:
				ternix->printoutAllOutputsConnectStatus();
				if((user_select = TernixConvertUserInputToUInt("Output ID:")) < 0)
					break;
				if((out = TernixOutput::getTernixOutput(user_select)) == NULL)
					break;
				if(out->getMode() != TERNIX_PROPO_OUTPUT) {
					std::cerr << "Output: " << (int)user_select << " is not in proportional mode." << std::endl;
					break;
				}
				std::cerr << "Output: " << (int)out->getId() << ", DC: " << (int)out->getPWMDC() << std::endl;
				if((user_select = TernixConvertUserInputToUInt("Duty cycle(1 ~ 100):")) < 0)
					break;
				out->setPWMDC(user_select);
				break;
			case TERNIX_USER_CMD_SET_PROPO_OUTPUT_CHANNEL_FREQ:
				ternix->printoutAllOutputsConnectStatus();
				if((user_select = TernixConvertUserInputToUInt("Output ID:")) < 0)
					break;
				if((out = TernixOutput::getTernixOutput(user_select)) == NULL)
					break;
				if(out->getMode() != TERNIX_PROPO_OUTPUT) {
					std::cerr << "Output: " << (int)user_select << " is not in proportional mode." << std::endl;
					break;
				}
				std::cerr << "Output: " << (int)out->getId() << ", Frequency: " << (int)out->getPWMFreq() << std::endl;
				if((user_select = TernixConvertUserInputToUInt("Frequency(1: 50HZ ~ 12: 600HZ):")) < 0)
					break;
				out->setPWMFreq(user_select);
				break;
			case TERNIX_USER_CMD_BOOTLOADER:
				if((user_select = TernixConvertUserInputToUInt("BootLoader Target modules(0: Propo, 1: Analog):")) < 0)
					break;

				if((bootloader = ternix->getI2cBootloader(user_select)) == NULL) {
					std::cerr << "Please select a valid module(0: Propo, 1: Analog)!" << std::endl;
					break;
				}

				if(!captureUserInputWithTimeout("Hex file path:", in_buff, 15))
					break;

				dir_path = in_buff;

				if(!captureUserInputWithTimeout("Hex file Name:", in_buff, 15))
					break;

				hex_file_name = in_buff;

				if(!TernixHelpUserFindHexFile(dir_path, hex_file_name, found_path)) {
					cerr << "Can not found the file in the specified path!" << endl;
					break;
				}
				cerr << "Hex File found: " << found_path.string() << endl;
				parser = new HexParser();
				parser->setHexFileName(found_path.string());
				parser->readHexFile();

				if(parser->isHexImageReady()) {
					bootloader->setHexParser(parser);
					if(user_select == TERNIX_ANALOG_MODULE)
						ternix->printOutAllAnalogModulesConnectStatus();
					else if(user_select == TERNIX_PROPO_MODULE)
						ternix->printOutAllPropoModulesConnectStatus();

					temp = user_select;

					if((user_select = TernixConvertUserInputToUInt("Select a Module to start bootloader.")) < 0) {
						cerr << "Please Input a valid module ID!" << endl;
						break;
					}

					if(temp == TERNIX_ANALOG_MODULE)
						addonModule = TernixTerminal::getAnalogModule(user_select);
					else if(temp == TERNIX_PROPO_MODULE)
						addonModule = TernixTerminal::getPropoModule(user_select);

					if(!addonModule->isConnected()) {
						cerr << "Please select a connected module!" << endl;
						break;
					}

					bootloader->bootloaderRun(addonModule->getI2cAddress());
				}
				else
					delete parser;
				break;
			case TERNIX_USRE_CMD_READ_ANALOG_INPUT:
				ternix->printoutAllInputsConnectStatus();

				if((user_select = TernixConvertUserInputToUInt("Select a Input to read analog data.")) < 0)
					break;

				if((in = TernixInput::getTernixInput(user_select)) == NULL) {
					cerr << "Wrong input ID!" << endl;
					break;
				}

				if(in->getMode() != TERNIX_ANALOG_INPUT) {
					cerr << "Wrong input ID!" << endl;
					break;
				}

				if(TernixTerminal::getAnalogModule(in->getModuleID())->readChannel(in->getChannelID(), data_buff) != I2CTask::Success) {
					cerr << "Read input: " << user_select << " Failed, please try again." << endl;
					break;
				}
				cerr << "Input: " << user_select << ", analog value: " << (((gushort)data_buff[1] << 8) | (data_buff[0])) << endl;
				break;
			case TERNIX_USER_CMD_NULL:
				break;
			default:
				cerr << "The selection is wrong!" << endl;
				break;
		}
		cerr << "\nPress \'Enter\' to give Command..." << endl;
		g_thread_yield();
    }
    return NULL;
}
