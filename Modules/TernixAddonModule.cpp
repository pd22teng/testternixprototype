#include "TernixAddonModule.h"

TernixAddonModule::TernixAddonModule(guchar id, TernixI2CBootLoader * bootloader, guchar i2cAddress)
{
    this->id = id;
    this->bootloader = bootloader;
    this->i2cAddress = i2cAddress;

    queryModuleI2cConnectState();
}

TernixAddonModule::~TernixAddonModule()
{
}

guchar TernixAddonModule::getId()
{
    return this->id;
}

guchar TernixAddonModule::getI2cAddress()
{
    return this->i2cAddress;
}

gboolean TernixAddonModule::isConnected()
{
    return this->connected;
}

I2CTask *TernixAddonModule::getI2cTask()
{
    return this->bootloader->getI2cTask();
}

void TernixAddonModule::setI2cAddress(guchar address)
{
    this->i2cAddress = address;
}

gboolean TernixAddonModule::queryModuleI2cConnectState()
{
    if(i2cAddress == 0)
        return FALSE;

    // send I2C address to query the connected status of the propo module
    if(bootloader->getI2cTask()->i2cIsDeviceReadyForSession(this->i2cAddress) == I2CTask::Success)
        this->connected = TRUE;
    else
        this->connected = FALSE;

    return this->connected;
}

