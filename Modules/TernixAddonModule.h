#ifndef TERNIXADDONMODULE_H
#define TERNIXADDONMODULE_H

#include "Ternix.h"
#include "TernixIO.h"
#include "TernixI2CBootloader.h"

class TernixAddonModule
{
public:
    TernixAddonModule(guchar id, TernixI2CBootLoader * bootloader, guchar i2cAddress);
    virtual ~TernixAddonModule();

    virtual void printOutAllChannelIDs() = 0;
    virtual I2CTask::ErrorCode i2cModuleInitConfig() = 0;
    virtual I2CTask::ErrorCode i2cUpdateChannelConfig(guchar chID) = 0;

    gboolean queryModuleI2cConnectState();

    void setI2cAddress(guchar address);

    guchar getId();
    guchar getI2cAddress();
    gboolean isConnected();
    I2CTask * getI2cTask();
private:
    guchar id;
    guchar i2cAddress;
    gboolean connected;
    TernixI2CBootLoader * bootloader;
};

#endif // TERNIXADDONMODULE_H
