#ifndef TERNIXPROPOMODULE_H
#define TERNIXPROPOMODULE_H

#include "TernixPropoModuleInfo.h"

class TernixPropoModule : public TernixAddonModule
{
public:
    TernixPropoModule(guchar id, TernixI2CBootLoader * bootloader);
    ~TernixPropoModule();

    I2CTask::ErrorCode TernixPropoModuleSetPWMDC(guchar channel, guint16 dc);
    I2CTask::ErrorCode TernixPropoModuleSetPWMFreq(guchar channel, guchar freq);

    I2CTask::ErrorCode i2cModuleInitConfig();
    I2CTask::ErrorCode i2cUpdateChannelConfig(guchar chID);

    TernixOutput * getOutputOfPropoModule(guchar channelID);
    void printOutAllChannelIDs();

private:
    TernixOutput * outputs[2];
};

#endif // TERNIXPROPOMODULE_H
