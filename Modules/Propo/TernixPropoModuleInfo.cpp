#include "TernixPropoModuleInfo.h"

const TernixPropoModuleInfoItem TernixPropoModuleInfo::TernixPropoModuleInfoTable[TERNIX_PROPO_MODULE_NUM] = {
    {TERNIX_PROPO_MODULE_1,  0x10, {TERNIX_OUTPUT_ID_1,  TERNIX_OUTPUT_ID_2 }}, {TERNIX_PROPO_MODULE_2,  0x11, {TERNIX_OUTPUT_ID_3,  TERNIX_OUTPUT_ID_4 }},
    {TERNIX_PROPO_MODULE_3,  0x12, {TERNIX_OUTPUT_ID_5,  TERNIX_OUTPUT_ID_6 }}, {TERNIX_PROPO_MODULE_4,  0x13, {TERNIX_OUTPUT_ID_7,  TERNIX_OUTPUT_ID_8 }},
    {TERNIX_PROPO_MODULE_5,  0x14, {TERNIX_OUTPUT_ID_9,  TERNIX_OUTPUT_ID_10}}, {TERNIX_PROPO_MODULE_6,  0x15, {TERNIX_OUTPUT_ID_11, TERNIX_OUTPUT_ID_12}},
    {TERNIX_PROPO_MODULE_7,  0x16, {TERNIX_OUTPUT_ID_13, TERNIX_OUTPUT_ID_14}}, {TERNIX_PROPO_MODULE_8,  0x17, {TERNIX_OUTPUT_ID_15, TERNIX_OUTPUT_ID_16}},
    {TERNIX_PROPO_MODULE_9,  0x18, {TERNIX_OUTPUT_ID_17, TERNIX_OUTPUT_ID_18}}, {TERNIX_PROPO_MODULE_10, 0x19, {TERNIX_OUTPUT_ID_19, TERNIX_OUTPUT_ID_20}},
    {TERNIX_PROPO_MODULE_11, 0x1a, {TERNIX_OUTPUT_ID_21, TERNIX_OUTPUT_ID_22}}, {TERNIX_PROPO_MODULE_12, 0x1b, {TERNIX_OUTPUT_ID_23, TERNIX_OUTPUT_ID_24}},
    {TERNIX_PROPO_MODULE_13, 0x1c, {TERNIX_OUTPUT_ID_25, TERNIX_OUTPUT_ID_26}}, {TERNIX_PROPO_MODULE_14, 0x1d, {TERNIX_OUTPUT_ID_27, TERNIX_OUTPUT_ID_28}},
    {TERNIX_PROPO_MODULE_15, 0x1e, {TERNIX_OUTPUT_ID_29, TERNIX_OUTPUT_ID_30}}, {TERNIX_PROPO_MODULE_16, 0x1f, {TERNIX_OUTPUT_ID_31, TERNIX_OUTPUT_ID_32}}
};

TernixPropoModuleInfo::TernixPropoModuleInfo()
{
}

guchar TernixPropoModuleInfo::getI2CAddressOfModuleItem(guchar id)
{
    if(id > TERNIX_PROPO_MODULE_NUM)
        return 0;

    return TernixPropoModuleInfoTable[id - 1].i2cAddress;
}

guchar TernixPropoModuleInfo::getModuleIdOfOutput(gchar outputId)
{
//    for(int i = 0; i < TERNIX_PROPO_MODULE_NUM; i++) {
//        for(int j = 0 ; j < TERNIX_PROPO_MODULE_CHANNELS; j++) {
//            if(TernixPropoModuleI2CAddress[i].propoChannels[j] == outputId)
//                return TernixPropoModuleI2CAddress[i].id;
//        }
//    }

    return (outputId - 1) / 2;
}

guchar TernixPropoModuleInfo::getChannelIdOfOutput(gchar outputId)
{
    if(outputId % 2 == 0)
        return TERNIX_PROPO_MODULE_CHANNEL_TWO;
    else
        return TERNIX_PROPO_MODULE_CHANNEL_ONE;
}

guchar TernixPropoModuleInfo::getI2CAddressOfOutput(gchar outputId)
{
    guchar moduleId = TERNIX_PROPO_MODULE_NULL;

    if((moduleId = getModuleIdOfOutput(outputId)) == TERNIX_PROPO_MODULE_NULL)
        return 0;
    else
        return TernixPropoModuleInfoTable[moduleId - 1].i2cAddress;
}

const guchar *TernixPropoModuleInfo::getOutputChannelsOfModule(guchar id)
{
    return &TernixPropoModuleInfoTable[id - 1].propoChannels[0];
}


