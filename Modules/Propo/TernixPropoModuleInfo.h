#ifndef TERNIXPROPOMODULEINFO_H
#define TERNIXPROPOMODULEINFO_H

#include "Ternix.h"
#include "../TernixAddonModule.h"
#include "IO/Outputs/TernixOutput.h"

#define TERNIX_PROPO_MODULE_I2C_BUS    I2C_BUS_2

#define TERNIX_PROPO_MODULE_NUM        16
#define TERNIX_PROPO_MODULE_CHANNELS   2

#define TERNIX_PROPO_MODULE_CHANNEL_ONE   1
#define TERNIX_PROPO_MODULE_CHANNEL_TWO   2

#define TERNIX_PROPO_MODULE_NULL    0
#define TERNIX_PROPO_MODULE_1       1
#define TERNIX_PROPO_MODULE_2       2
#define TERNIX_PROPO_MODULE_3       3
#define TERNIX_PROPO_MODULE_4       4
#define TERNIX_PROPO_MODULE_5       5
#define TERNIX_PROPO_MODULE_6       6
#define TERNIX_PROPO_MODULE_7       7
#define TERNIX_PROPO_MODULE_8       8
#define TERNIX_PROPO_MODULE_9       9
#define TERNIX_PROPO_MODULE_10      10
#define TERNIX_PROPO_MODULE_11      11
#define TERNIX_PROPO_MODULE_12      12
#define TERNIX_PROPO_MODULE_13      13
#define TERNIX_PROPO_MODULE_14      14
#define TERNIX_PROPO_MODULE_15      15
#define TERNIX_PROPO_MODULE_16      16

typedef struct {
    guchar id;
    guchar i2cAddress;
    guchar propoChannels[TERNIX_PROPO_MODULE_CHANNELS];
} TernixPropoModuleInfoItem;

class TernixPropoModuleInfo
{
public:
    TernixPropoModuleInfo();

    static guchar getI2CAddressOfModuleItem(guchar id);
    static guchar getModuleIdOfOutput(gchar outputId);
    static guchar getChannelIdOfOutput(gchar outputId);
    static guchar getI2CAddressOfOutput(gchar outputId);
    static const guchar * getOutputChannelsOfModule(guchar id);

private:
    static const TernixPropoModuleInfoItem TernixPropoModuleInfoTable[TERNIX_PROPO_MODULE_NUM];
};

#endif // TERNIXPROPOMODULEINFO_H
