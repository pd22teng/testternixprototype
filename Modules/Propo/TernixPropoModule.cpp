#include "TernixPropoModule.h"

TernixPropoModule::TernixPropoModule(guchar id, TernixI2CBootLoader * bootloader)
    :TernixAddonModule(id, bootloader, TernixPropoModuleInfo::getI2CAddressOfModuleItem(id))
{
    guchar mode = TERNIX_DIGIT_OUTPUT;

    if(isConnected()) mode = TERNIX_PROPO_OUTPUT;

    // Outputs
    const guchar *pCh = TernixPropoModuleInfo::getOutputChannelsOfModule(id);

    for(int i = 0; i < TERNIX_PROPO_MODULE_CHANNELS; i++) {
        guchar outputId = pCh[i];
        guchar PropoChannelID = TernixPropoModuleInfo::getChannelIdOfOutput(outputId);
        outputs[i] = new TernixOutput(outputId, TERNIX_ENABLE_OUTPUT, mode, getId(), PropoChannelID, TERNIX_OUTPUT_DEF_OC_THRESHOLD);  //outputId, enable, mode, digit, propModuleID, PropoChannelID
    }

    if(isConnected()) {
        if(this->i2cModuleInitConfig() != I2CTask::Success)
            std::cerr << "Propo module : " << (int)getId() << " init config failed!" << std::endl;
    }
}

TernixPropoModule::~TernixPropoModule()
{
    // Delete Outputs
    for(int i = 0; i < TERNIX_PROPO_MODULE_CHANNELS; i++) {
        if(outputs[i] != NULL) {
            delete outputs[i];
            outputs[i] = NULL;
        }
    }
}

I2CTask::ErrorCode TernixPropoModule::TernixPropoModuleSetPWMDC(guchar channel, guint16 dc)
{
    guchar tdata[I2C_TERNIX_PROPO_MODULE_PWM_DC_MSG_LEN];
    tdata[0] = channel;
    tdata[1] = dc & 0x00ff;
    tdata[2] = dc >> 8;

    return getI2cTask()->i2cStartNewSession(getI2cAddress(), I2C_TERNIX_CMD_PROPO_CHANNEL_SET_PWM_DC, I2C_TERNIX_PROPO_MODULE_PWM_DC_MSG_LEN, I2C_TERNIX_NULL_MESSAGE_LEN, tdata, NULL);
}

I2CTask::ErrorCode TernixPropoModule::TernixPropoModuleSetPWMFreq(guchar channel, guchar freq)
{
    guchar tdata[I2C_TERNIX_PROPO_MODULE_PWM_FREQ_MSG_LEN];
    tdata[0] = channel;
    tdata[1] = freq;

    return getI2cTask()->i2cStartNewSession(getI2cAddress(), I2C_TERNIX_CMD_PROPO_CHANNEL_SET_PWM_FREQ, I2C_TERNIX_PROPO_MODULE_PWM_FREQ_MSG_LEN, I2C_TERNIX_NULL_MESSAGE_LEN, tdata, NULL);
}

I2CTask::ErrorCode TernixPropoModule::i2cModuleInitConfig()
{
    // Send the init setup msg to each channels of the module
    guchar tdata[TERNIX_PROPO_MODULE_CHANNELS];
    tdata[0] = outputs[0]->getChannelSetupMsg();
    tdata[1] = outputs[1]->getChannelSetupMsg();

    return getI2cTask()->i2cStartNewSession(getI2cAddress(), I2C_TERNIX_CMD_PROPO_MODULE_INIT_CONFIG, (TERNIX_PROPO_MODULE_CHANNELS * I2C_TERNIX_ANALOG_MODULE_INIT_CONFIG_MSG_LEN), I2C_TERNIX_NULL_MESSAGE_LEN, tdata, NULL);
}

I2CTask::ErrorCode TernixPropoModule::i2cUpdateChannelConfig(guchar chID)
{
    TernixOutput * out = getOutputOfPropoModule(chID);

    guchar tdata = out->getChannelSetupMsg();

    return getI2cTask()->i2cStartNewSession(getI2cAddress(), I2C_TERNIX_CMD_PROPO_CHANNEL_CONFIG, I2C_TERNIX_PROPO_MODULE_INIT_CONFIG_MSG_LEN, I2C_TERNIX_NULL_MESSAGE_LEN, &tdata, NULL);
}

TernixOutput *TernixPropoModule::getOutputOfPropoModule(guchar channelID)
{
    if(channelID == TERNIX_PROPO_MODULE_CHANNEL_ONE)
        return outputs[0];
    else if(channelID == TERNIX_PROPO_MODULE_CHANNEL_TWO)
        return outputs[1];

    return 0;
}

void TernixPropoModule::printOutAllChannelIDs()
{
    std::cerr << (int)this->getOutputOfPropoModule(TERNIX_PROPO_MODULE_CHANNEL_ONE)->getId() << " "
              << (int)this->getOutputOfPropoModule(TERNIX_PROPO_MODULE_CHANNEL_TWO)->getId() << std::endl;;
}

