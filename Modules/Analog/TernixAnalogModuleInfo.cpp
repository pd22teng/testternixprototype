#include "TernixAnalogModuleInfo.h"

const TernixAnalogModuleInfoItem TernixAnalogModuleInfo::TernixAnalogModuleInfoTable[TERNIX_ANALOG_MODULE_NUM] =
{
    {TERNIX_ANALOG_MODULE_ONE, 0X11, 6, {TERNIX_INPUT_ID_1,  TERNIX_INPUT_ID_2,  TERNIX_INPUT_ID_3,  TERNIX_INPUT_ID_4,  TERNIX_INPUT_ID_5,  TERNIX_INPUT_ID_6}},
    {TERNIX_ANALOG_MODULE_TWO, 0X12, 5, {TERNIX_INPUT_ID_7,  TERNIX_INPUT_ID_8,  TERNIX_INPUT_ID_9,  TERNIX_INPUT_ID_10, TERNIX_INPUT_ID_11, TERNIX_INPUT_NULL}},
    {TERNIX_ANALOG_MODULE_THR, 0X13, 6, {TERNIX_INPUT_ID_12, TERNIX_INPUT_ID_13, TERNIX_INPUT_ID_14, TERNIX_INPUT_ID_15, TERNIX_INPUT_ID_16, TERNIX_INPUT_ID_17}},
    {TERNIX_ANALOG_MODULE_FOU, 0X14, 5, {TERNIX_INPUT_ID_18, TERNIX_INPUT_ID_19, TERNIX_INPUT_ID_20, TERNIX_INPUT_ID_21, TERNIX_INPUT_ID_22, TERNIX_INPUT_NULL}},
    {TERNIX_ANALOG_MODULE_FIV, 0X15, 5, {TERNIX_INPUT_ID_23, TERNIX_INPUT_ID_24, TERNIX_INPUT_ID_25, TERNIX_INPUT_ID_26, TERNIX_INPUT_ID_27, TERNIX_INPUT_NULL}},
    {TERNIX_ANALOG_MODULE_SIX, 0X16, 5, {TERNIX_INPUT_ID_28, TERNIX_INPUT_ID_29, TERNIX_INPUT_ID_30, TERNIX_INPUT_ID_31, TERNIX_INPUT_ID_32, TERNIX_INPUT_NULL}}
};

TernixAnalogModuleInfo::TernixAnalogModuleInfo()
{
}

guchar TernixAnalogModuleInfo::getI2cAddressOfModule(guchar id)
{
    if(id > TERNIX_ANALOG_MODULE_NUM)
        return 0;

    return TernixAnalogModuleInfoTable[id -1].i2cAddress;
}

guchar TernixAnalogModuleInfo::getModuleIdOfInput(gchar inputId)
{
    guchar inputSum = 0;

    for(gint i = 0; i < TERNIX_ANALOG_MODULE_NUM; i++) {
        inputSum += TernixAnalogModuleInfoTable[i].channelNum;

        if(inputId <= inputSum)
            return TernixAnalogModuleInfoTable[i].id;
    }

    return 0;
}

guchar TernixAnalogModuleInfo::getModuleIdAndChannelIdOfInput(gchar inputId, guchar * channel)
{
    guchar inputSum = 0;

    for(gint i = 0; i < TERNIX_ANALOG_MODULE_NUM; i++) {
        inputSum += TernixAnalogModuleInfoTable[i].channelNum;

        if(inputId <= inputSum) {
            *channel = TernixAnalogModuleInfoTable[i].channelNum - (inputSum - inputId);
            return TernixAnalogModuleInfoTable[i].id;
        }
    }

    return 0;
}

guchar TernixAnalogModuleInfo::getI2CAddressOfInput(gchar inputId)
{
    guchar moduleId = 0;

    moduleId = getModuleIdOfInput(inputId);

    return getI2cAddressOfModule(moduleId);
}

guchar TernixAnalogModuleInfo::getInputChannelNumOfModule(guchar id)
{
    return TernixAnalogModuleInfoTable[id - 1].channelNum;
}

const guchar *TernixAnalogModuleInfo::getInputChannelsOfModule(guchar id)
{
    return TernixAnalogModuleInfoTable[id - 1].analogChannels;
}
