#ifndef TERNIXANALOGMODULEINFO_H
#define TERNIXANALOGMODULEINFO_H

#include "Ternix.h"
#include "../TernixAddonModule.h"
#include "IO/Inputs/TernixInput.h"

#define TERNIX_SENSOR_MODULE_I2C_BUS        I2C_BUS_1

#define TERNIX_ANALOG_MODULE_NUM            6
#define TERNIX_ANALOG_MODULE_MAX_CHANNELS   6

#define TERNIX_ANALOG_MODULE_NULL   0
#define TERNIX_ANALOG_MODULE_ONE    1
#define TERNIX_ANALOG_MODULE_TWO    2
#define TERNIX_ANALOG_MODULE_THR    3
#define TERNIX_ANALOG_MODULE_FOU    4
#define TERNIX_ANALOG_MODULE_FIV    5
#define TERNIX_ANALOG_MODULE_SIX    6

typedef struct {
    guchar id;
    guchar i2cAddress;
    guchar channelNum;
    guchar analogChannels[TERNIX_ANALOG_MODULE_MAX_CHANNELS];
} TernixAnalogModuleInfoItem;

class TernixAnalogModuleInfo
{
public:
    TernixAnalogModuleInfo();

    static guchar getI2cAddressOfModule(guchar id);
    static guchar getI2CAddressOfInput(gchar inputId);
    static guchar getModuleIdOfInput(gchar inputId);
    static guchar getModuleIdAndChannelIdOfInput(gchar inputId, guchar * channel);
    static guchar getInputChannelNumOfModule(guchar id);
    static const guchar * getInputChannelsOfModule(guchar id);

private:
    static const TernixAnalogModuleInfoItem TernixAnalogModuleInfoTable[TERNIX_ANALOG_MODULE_NUM];
};

#endif // TERNIXANALOGMODULEINFO_H
