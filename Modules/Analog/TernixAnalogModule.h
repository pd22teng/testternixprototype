#ifndef TERNIXANALOGMODULE_H
#define TERNIXANALOGMODULE_H

#include "TernixAnalogModuleInfo.h"

class TernixAnalogModule : public TernixAddonModule
{
public:
    explicit TernixAnalogModule(guchar id, TernixI2CBootLoader * bootloader);
    ~TernixAnalogModule();

    I2CTask::ErrorCode readChannel(guchar channel, guchar * pdata);
    I2CTask::ErrorCode queryUpdateStatus(guchar * statusByte);
    I2CTask::ErrorCode readUpdatedChannelsDatas(guchar len, guchar *pdata);

    I2CTask::ErrorCode i2cModuleInitConfig();
    I2CTask::ErrorCode i2cUpdateChannelConfig(guchar chID);

    TernixInput * getInputOfAnalogModule(guchar ChannelId);

    static const guint ternixAnalogModulesInterruptGPIO;
    static gboolean AnalogUpdateGpioEventHandler(GIOChannel* channel, GIOCondition condition, gpointer user_data);

    gboolean isSensorDataUpdated();
    void setSensorDataUpdatedFlag();
    void clearSensorDataUpdateFlag();
    void lockSensorDataUpdateMutex();
    void unlockSensorDataUpdateMutex();
    void waitForSensorDataUpdateCond();

    void printOutAllChannelIDs();

private:
    TernixInput * inputs[6];

    gboolean dataUpdated;
    GMutex updateMutex;
    GCond updateCond;

    static gpointer TernixAnalogModulesThreadFunc(gpointer data);
};

#endif // TERNIXANALOGMODULE_H
