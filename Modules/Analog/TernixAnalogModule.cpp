#include "TernixAnalogModule.h"
#include <iostream>
#include "TernixTerminal.h"

const guint TernixAnalogModule::ternixAnalogModulesInterruptGPIO = TERNIX_ANALOG_INPUT_CHANGED_INT;

TernixAnalogModule::TernixAnalogModule(guchar id, TernixI2CBootLoader * bootloader)
    :TernixAddonModule(id, bootloader, TernixAnalogModuleInfo::getI2cAddressOfModule(id))
{
    guchar mode = TERNIX_DIGITAL_INPUT;

    if(isConnected()) mode = TERNIX_ANALOG_INPUT;

    dataUpdated = false;
    g_mutex_init(&updateMutex);
    g_cond_init(&updateCond);

    // Inputs
    guchar chNum = TernixAnalogModuleInfo::getInputChannelNumOfModule(id);
    const guchar *pChs = TernixAnalogModuleInfo::getInputChannelsOfModule(id);

    for(int i = 0; i < chNum; i++) {
        inputs[i] = new TernixInput(pChs[i], TERNIX_ENABLE_INPUT, mode, getId(), i + 1);
    }

    if(isConnected()) {
        if(this->i2cModuleInitConfig() != I2CTask::Success)
            std::cerr << "Analog module : " << (int)getId() << " init config failed!" << std::endl;
    }

    g_thread_new("Ternix analog module thread " + getId(), TernixAnalogModule::TernixAnalogModulesThreadFunc, (gpointer)this);
}

TernixAnalogModule::~TernixAnalogModule()
{
    // Delete Inputs
    guchar chNum = TernixAnalogModuleInfo::getInputChannelNumOfModule(getId());

    for(int i = 0; i < chNum; i++) {
        if(inputs[i] != NULL) {
            delete inputs[i];
            inputs[i] = NULL;
        }
    }
}

TernixInput *TernixAnalogModule::getInputOfAnalogModule(guchar ChannelId)
{
    if(ChannelId <= TernixAnalogModuleInfo::getInputChannelNumOfModule(getId()))
        return inputs[ChannelId - 1];
    return NULL;
}

void TernixAnalogModule::printOutAllChannelIDs()
{
    guchar num = TernixAnalogModuleInfo::getInputChannelNumOfModule(getId());

    for(int i = 0; i < num; i++)
        std::cerr << (int)inputs[i]->getId() << " ";
    std::cerr << std::endl;
}

void TernixAnalogModule::setSensorDataUpdatedFlag()
{
	g_mutex_lock(&updateMutex);
	if(!dataUpdated) {
		dataUpdated = true;
		g_cond_signal(&updateCond);
	}
	g_mutex_unlock(&updateMutex);
}

gboolean TernixAnalogModule::isSensorDataUpdated()
{
    return dataUpdated;
}

void TernixAnalogModule::clearSensorDataUpdateFlag()
{
	g_mutex_lock(&updateMutex);
	dataUpdated = false;
	g_mutex_unlock(&updateMutex);
}

void TernixAnalogModule::lockSensorDataUpdateMutex()
{
	g_mutex_lock(&updateMutex);
}

void TernixAnalogModule::unlockSensorDataUpdateMutex()
{
	g_mutex_unlock(&updateMutex);
}

void TernixAnalogModule::waitForSensorDataUpdateCond()
{
	g_cond_wait(&updateCond, &updateMutex);
}
gboolean TernixAnalogModule::AnalogUpdateGpioEventHandler(GIOChannel *channel, GIOCondition condition, gpointer user_data)
{
    guint val = 0;
    gpio_get_value(TernixAnalogModule::ternixAnalogModulesInterruptGPIO, &val);

    if(val) {
    	for(guchar i = TERNIX_ANALOG_MODULE_ONE; i <= TERNIX_ANALOG_MODULE_SIX; i++)
    		TernixTerminal::getAnalogModule(i)->setSensorDataUpdatedFlag();
    }

    return TRUE;
}

I2CTask::ErrorCode TernixAnalogModule::readChannel(guchar channel, guchar * pdata)
{
    return getI2cTask()->i2cStartNewSession(getI2cAddress(), I2C_TERNIX_CMD_ANALOG_SENSOR_DATA_READ, I2C_TERNIX_ANANLOG_SENSOR_CHANNEL_MSG_LEN, I2C_TERNIX_ANANLOG_SENSOR_DATA_MSG_LEN, &channel, pdata);
}

I2CTask::ErrorCode TernixAnalogModule::queryUpdateStatus(guchar * statusByte)
{
    return getI2cTask()->i2cStartNewSession(getI2cAddress(), I2C_TERNIX_CMD_ANALOG_MODULE_UPATE_STATE_QUERY, I2C_TERNIX_NULL_MESSAGE_LEN, I2C_TERNIX_ANALOG_MODULE_UPDATE_STATUS_MSG_LEN, NULL, statusByte);
}

I2CTask::ErrorCode TernixAnalogModule::readUpdatedChannelsDatas(guchar len, guchar *pdata)
{
    return getI2cTask()->i2cStartNewSession(getI2cAddress(), I2C_TERNIX_CMD_ANALOG_MODULE_READ_UPDATED_CHANNELS, I2C_TERNIX_NULL_MESSAGE_LEN, len, NULL, pdata);
}

I2CTask::ErrorCode TernixAnalogModule::i2cModuleInitConfig()
{
    // Send the init setup msg to each channels of the module
    guchar chNum = TernixAnalogModuleInfo::getInputChannelNumOfModule(getId());
    guchar tdata[TERNIX_ANALOG_MODULE_MAX_CHANNELS];

    for(int i = 0 ; i < TERNIX_ANALOG_MODULE_MAX_CHANNELS; i++) {
        if(i < chNum)
            tdata[i] = inputs[i]->getChannelSetupMsg();
        else
            tdata[i] = 0;
    }

    return getI2cTask()->i2cStartNewSession(getI2cAddress(), I2C_TERNIX_CMD_ANALOG_MODULE_INIT_CONFIG, chNum * I2C_TERNIX_PROPO_MODULE_INIT_CONFIG_MSG_LEN, I2C_TERNIX_NULL_MESSAGE_LEN, tdata, NULL);
}

I2CTask::ErrorCode TernixAnalogModule::i2cUpdateChannelConfig(guchar chID)
{
    TernixInput * in = getInputOfAnalogModule(chID);

    guchar tdata = in->getChannelSetupMsg();

    return getI2cTask()->i2cStartNewSession(getI2cAddress(), I2C_TERNIX_CMD_ANALOG_CHANNEL_CONFIG, I2C_TERNIX_ANALOG_MODULE_INIT_CONFIG_MSG_LEN, I2C_TERNIX_NULL_MESSAGE_LEN, &tdata, NULL);
}

gpointer TernixAnalogModule::TernixAnalogModulesThreadFunc(gpointer data)
{
    TernixAnalogModule * analog = (TernixAnalogModule *)data;
    guchar dbyte = 0;
//    guchar channelNum = 0
    TernixInput * input = NULL;
    guchar channeldata[12];
    gushort analogValue = 0;

    while(TRUE) {
        analog->lockSensorDataUpdateMutex();
        while(!analog->isSensorDataUpdated())
        	analog->waitForSensorDataUpdateCond();
        analog->unlockSensorDataUpdateMutex();

        analog->clearSensorDataUpdateFlag();

//        channelNum = 0;

        if(analog->isConnected()) { // Only update the connected analog module
            if(analog->queryUpdateStatus(&dbyte) == I2CTask::Success) {
                // calculate number of updated channels
            	if(dbyte) {
            		for(int i = 0; i < 6; i++) {
						if(dbyte & (1 << i)) {
							if(analog->readChannel(i + 1, channeldata) == I2CTask::Success) {
								analogValue = channeldata[0] | ((gushort)channeldata[1]) << 8;
								input = analog->getInputOfAnalogModule(i + 1);
								input->setAnalogVal(analogValue);
								std::cout << "Analog Module: " << (int)analog->getId() << ", Channel: " << (int)input->getChannelID() << ", analog value updated to " << (int)analogValue << std::endl;
							}
//							std::cout << "Analog Module: " << analog->getId() << ", Channel " << (i + 1) << " updated!" << std::endl;
//							channelNum++;
						}
					}

//                    if(analog->readUpdatedChannelsDatas(2 * channelNum, channeldata) == I2CTask::Success) {
//                        channelNum = TernixAnalogModuleInfo::getInputChannelNumOfModule(analog->getId());
//
//                        for(int i = 0, j = 0; i < channelNum; i++) {
//                            if(dbyte & (1 << i)) {
//                            	analogValue = channeldata[j] | ((gushort)channeldata[j + 1]) << 8;
//                            	analog->getInputOfAnalogModule(i + 1)->setAnalogVal(analogValue);
//                            	std::cout << "Analog Module: " << (int)analog->getId() << " analog value updated to " << (int)analogValue << std::endl;
//                            	j += 2;
//                            }
//                        }
//                    }
            	}
            }
        }
        g_thread_yield();
    }

    return NULL;
}
