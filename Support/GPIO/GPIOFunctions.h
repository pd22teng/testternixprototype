#ifndef FUNCTIONGPIO_H_
#define FUNCTIONGPIO_H_

//#define _SAMA_DEVELOP_BOARD_

#include "AT91GPIO.h"
#include <glib-2.0/glib.h>
 /****************************************************************
 * Constants
 ****************************************************************/

#define SYSFS_GPIO_DIR "/sys/class/gpio"
#define POLL_TIMEOUT (3 * 1000) /* 3 seconds */
#define MAX_BUF 64

enum PIN_DIRECTION {
    INPUT_PIN = 0,
    OUTPUT_PIN = 1
};

enum PIN_VALUE{
    LOW = 0,
    HIGH = 1
};

int gpio_set_export(unsigned int gpio);
int gpio_set_unexport(unsigned int gpio);
int gpio_set_dir(unsigned int gpio, PIN_DIRECTION out_flag);
int gpio_set_value(unsigned int gpio, PIN_VALUE value);
int gpio_get_value(unsigned int gpio, unsigned int *value);
int gpio_write_fd_value(int fd, unsigned int value);
int gpio_read_fd_value(int fd, unsigned int *value);
int gpio_set_edge(unsigned int gpio, char *edge);
int gpio_fd_open(unsigned int gpio);
int gpio_fd_close(int fd);

int gpio_set_input_mode(unsigned int gpio);
int gpio_set_output_mode(unsigned int gpio);
int gpio_set_glib_event_handler(unsigned int gpio, GIOFunc eventHandler, gpointer user_data);

#endif /* FUNCTIONGPIO_H_ */
