#include "GPIOFunctions.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <poll.h>

/****************************************************************
 * gpio_export
 ****************************************************************/
int gpio_set_export(unsigned int gpio)
{
	int fd, len;
	char buf[MAX_BUF];

	fd = open(SYSFS_GPIO_DIR "/export", O_WRONLY);
	if (fd < 0) {
		perror("gpio/export");
		return fd;
	}

	len = snprintf(buf, sizeof(buf), "%d", gpio);
	write(fd, buf, len);
	close(fd);

	return 0;
}

/****************************************************************
 * gpio_unexport
 ****************************************************************/
int gpio_set_unexport(unsigned int gpio)
{
	int fd, len;
	char buf[MAX_BUF];

	fd = open(SYSFS_GPIO_DIR "/unexport", O_WRONLY);
	if (fd < 0) {
		perror("gpio/export");
		return fd;
	}

	len = snprintf(buf, sizeof(buf), "%d", gpio);
	write(fd, buf, len);
	close(fd);

	return 0;
}

/****************************************************************
 * gpio_set_dir
 ****************************************************************/
int gpio_set_dir(unsigned int gpio, PIN_DIRECTION out_flag)
{
	int fd;
	char buf[MAX_BUF];

#ifndef _SAMA_DEVELOP_BOARD_
    snprintf(buf, sizeof(buf), SYSFS_GPIO_DIR  "/pio%s/direction", at91_gpio_str[gpio]);
#else
    snprintf(buf, sizeof(buf), SYSFS_GPIO_DIR  "/gpio%d/direction", gpio);
#endif
	fd = open(buf, O_WRONLY);
	if (fd < 0) {
        perror(&buf[0]);
		return fd;
	}

	if (out_flag == OUTPUT_PIN)
		write(fd, "out", 4);
	else
		write(fd, "in", 3);

	close(fd);
	return 0;
}

/****************************************************************
 * gpio_set_value
 ****************************************************************/
int gpio_set_value(unsigned int gpio, PIN_VALUE value)
{
	int fd;
	char buf[MAX_BUF];

#ifndef _SAMA_DEVELOP_BOARD_
    snprintf(buf, sizeof(buf), SYSFS_GPIO_DIR "/pio%s/value", at91_gpio_str[gpio]);
#else
    snprintf(buf, sizeof(buf), SYSFS_GPIO_DIR  "/gpio%d/value", gpio);
#endif

	fd = open(buf, O_WRONLY);
	if (fd < 0) {
        perror(&buf[0]);
		return fd;
	}

	if (value==LOW)
		write(fd, "0", 2);
	else
		write(fd, "1", 2);

	close(fd);
	return 0;
}

/****************************************************************
 * gpio_get_value
 ****************************************************************/
int gpio_get_value(unsigned int gpio, unsigned int *value)
{
	int fd;
	char buf[MAX_BUF];
	char ch;

#ifndef _SAMA_DEVELOP_BOARD_
    snprintf(buf, sizeof(buf), SYSFS_GPIO_DIR "/pio%s/value", at91_gpio_str[gpio]);
#else
    snprintf(buf, sizeof(buf), SYSFS_GPIO_DIR  "/gpio%d/value", gpio);
#endif

	fd = open(buf, O_RDONLY);
	if (fd < 0) {
        perror(&buf[0]);
		return fd;
	}

	read(fd, &ch, 1);

	if (ch != '0') {
		*value = 1;
	} else {
		*value = 0;
	}

	close(fd);
	return 0;
}


/****************************************************************
 * gpio_set_edge
 ****************************************************************/

int gpio_set_edge(unsigned int gpio, char *edge)
{
	int fd;
	char buf[MAX_BUF];

#ifndef _SAMA_DEVELOP_BOARD_
    snprintf(buf, sizeof(buf), SYSFS_GPIO_DIR "/pio%s/edge", at91_gpio_str[gpio]);
#else
    snprintf(buf, sizeof(buf), SYSFS_GPIO_DIR  "/gpio%d/edge", gpio);
#endif

	fd = open(buf, O_WRONLY);
	if (fd < 0) {
        perror(&buf[0]);
		return fd;
	}

	write(fd, edge, strlen(edge) + 1);
	close(fd);
	return 0;
}

/****************************************************************
 * gpio_fd_open
 ****************************************************************/

int gpio_fd_open(unsigned int gpio)
{
	int fd;
	char buf[MAX_BUF];

#ifndef _SAMA_DEVELOP_BOARD_
    snprintf(buf, sizeof(buf), SYSFS_GPIO_DIR "/pio%s/value", at91_gpio_str[gpio]);
#else
    snprintf(buf, sizeof(buf), SYSFS_GPIO_DIR  "/gpio%d/value", gpio);
#endif

	fd = open(buf, O_RDONLY | O_NONBLOCK );
	if (fd < 0) {
        perror(&buf[0]);
	}
	return fd;
}

/****************************************************************
 * gpio_fd_close
 ****************************************************************/

int gpio_fd_close(int fd)
{
	return close(fd);
}


int gpio_read_fd_value(int fd, unsigned int *value)
{
    char ch;

    if (fd < 0) {
        perror("fd error!");
        return fd;
    }

    read(fd, &ch, 1);

    if (ch != '0') {
        *value = 1;
    } else {
        *value = 0;
    }

//    close(fd);
    return 0;
}

int gpio_write_fd_value(int fd, unsigned int value)
{
    if (fd < 0) {
        perror("fd error!");
        return fd;
    }

    if (value == LOW)
        write(fd, "0", 2);
    else
        write(fd, "1", 2);

//    close(fd);
    return 0;
}

int gpio_set_input_mode(unsigned int gpio)
{
    int result = 0;

    // first export gpio pin
    if((result = gpio_set_export(gpio)) < 0)
        return result;

    // set direction as input
    result = gpio_set_dir(gpio, INPUT_PIN);

    return result;
}

int gpio_set_output_mode(unsigned int gpio)
{
    int result = 0;

    // first export gpio pin
    if((result = gpio_set_export(gpio)) < 0)
        return result;

    // set direction as input
    result = gpio_set_dir(gpio, OUTPUT_PIN);

    result = gpio_set_value(gpio, LOW);

    return result;
}

int gpio_set_glib_event_handler(unsigned int gpio, GIOFunc eventHandler, gpointer user_data)
{
    gint fd = gpio_fd_open(gpio);

    if(fd < 0)
        return -1;

    GIOChannel* channel = g_io_channel_unix_new(fd);
    GIOCondition condition = G_IO_PRI;
    g_io_add_watch(channel, condition, eventHandler, user_data);

    return 0;
}

