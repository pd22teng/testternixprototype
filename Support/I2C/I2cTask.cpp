#include "I2cTask.h"

#define _DO_STH_IN_SEQ_HEAD_    do
#define _DO_STH_IN_SEQ_END_     while(0)
#define _WAIT_TO_FINISH_        while

const I2CMsgBuff I2CTask::INITAILIZED_BUFF = {0, 0, {0,0}, {0,0}};

I2CTask::I2CTask(int i2cBus)
{
    drv = new I2CDriver(i2cBus);
}

I2CTask::~I2CTask()
{
	if(drv != NULL) {
		delete drv;
		drv = NULL;
	}
}

I2CTask::ErrorCode I2CTask::i2cRunSingleSessionTask(I2CMsgBuff *buff)
{
    ErrorCode result = Success;

    if((drv->i2c_msg_transceiver(buff)) < 0)
        result = DataTransferFail;

    return result;
}

void I2CTask::i2cNewSessionMessageInit(I2CMsgBuff * i2cMsg, guchar i2cAddress, guchar cmd, guchar tLen, guchar rLen, guchar *tBuff, guchar *rBuff)
{
    i2cMsg->i2cAddress = i2cAddress;
    i2cMsg->cmd = cmd;
    i2cMsg->rBuf.Len = rLen;
    i2cMsg->rBuf.Data = rBuff;
    i2cMsg->tBuf.Len = tLen;
    i2cMsg->tBuf.Data = tBuff;
}

I2CTask::ErrorCode I2CTask::i2cStartNewSession(guchar i2cAddress, guchar cmd, guchar tLen, guchar rLen, guchar *tBuff, guchar *rBuff)
{
    ErrorCode result = I2CTask::Success;
    I2CMsgBuff buff = I2CTask::INITAILIZED_BUFF;

    i2cNewSessionMessageInit(&buff, i2cAddress, cmd, tLen, rLen, tBuff, rBuff);

    result = i2cRunSingleSessionTask(&buff);

    return result;
}

I2CTask::ErrorCode I2CTask::i2cIsDeviceReadyForSession(guchar i2cAddress)
{
    ErrorCode result = I2CTask::Success;
    I2CMsgBuff buff = I2CTask::INITAILIZED_BUFF;

    buff.i2cAddress = i2cAddress;

    result = i2cRunSingleSessionTask(&buff);

    return result;
}

//I2CTask::ErrorCode I2CTask::I2CTernixAnalogSensorsReadChannel(guchar i2cAddress, guchar ch, guchar *rData)
//{
//    ErrorCode result = I2CTask::Success;
//    I2CMsgBuff buff = I2CTask::INITAILIZED_BUFF;

//    guchar data[8];

//    for(int i = 0; i < 8; i++)
//        data[i] = 0;

//    data[0] = ch;

//    buff.i2cAddress = i2cAddress;
//    buff.cmd = I2C_TERNIX_CMD_ANALOG_SENSOR_DATA_READ;
//    buff.tBuf.Len = I2C_TERNIX_ANANLOG_SENSOR_CHANNEL_MSG_LEN;
//    buff.tBuf.Data = data;
//    buff.rBuf.Len = I2C_TERNIX_ANANLOG_SENSOR_DATA_MSG_LEN;
//    buff.rBuf.Data = rData;

//    result = I2CTernixRunSingleSessionTask(&buff);

//    return result;
//}

//I2CTask::ErrorCode I2CTask::I2CTernixBootLoaderSendWaitCmd(guchar i2cAddress)
//{
//    ErrorCode result = I2CTask::Success;
//    I2CMsgBuff buff = I2CTask::INITAILIZED_BUFF;

//    buff.i2cAddress = i2cAddress;
//    buff.cmd = I2C_TERNIX_CMD_BOOTLOADER_DATA_WAIT;
//    result = I2CTernixRunSingleSessionTask(&buff);

//    return result;
//}

//I2CTask::ErrorCode I2CTask::I2CTernixBootLoaderSendResetCmd(guchar i2cAddress)
//{
//    ErrorCode result = I2CTask::Success;
//    I2CMsgBuff buff = I2CTask::INITAILIZED_BUFF;

//    buff.i2cAddress = i2cAddress;
//    buff.cmd = I2C_TERNIX_CMD_BOOTLOADER_RESET;
//    result = I2CTernixRunSingleSessionTask(&buff);

//    return result;
//}

//I2CTask::ErrorCode I2CTask::I2CTernixBootLoaderSendCRCCalculateCmd(guchar i2cAddress)
//{
//    ErrorCode result = I2CTask::Success;
//    I2CMsgBuff buff = I2CTask::INITAILIZED_BUFF;

//    buff.i2cAddress = i2cAddress;
//    buff.cmd = I2C_TERNIX_CMD_BOOTLOADER_CRC_CALCULATE;
//    result = I2CTernixRunSingleSessionTask(&buff);

//    return result;
//}

//I2CTask::ErrorCode I2CTask::I2CTernixBootLoaderQueryState(guchar i2cAddress)
//{
//    ErrorCode result = I2CTask::Success;
//    I2CMsgBuff buff = I2CTask::INITAILIZED_BUFF;

//    buff.i2cAddress = i2cAddress;
//    buff.cmd = I2C_TERNIX_CMD_BOOTLOADER_QUERY_STATE;
//    result = I2CTernixRunSingleSessionTask(&buff);

//    return result;
//}

//I2CTask::ErrorCode I2CTask::I2CTernixBootLoaderReadCRC(guchar i2cAddress, guchar *rData)
//{
//    ErrorCode result = I2CTask::Success;
//    I2CMsgBuff buff = I2CTask::INITAILIZED_BUFF;

//    buff.i2cAddress = i2cAddress;
//    buff.cmd = I2C_TERNIX_CMD_BOOTLOADER_CRC_READ;
//    buff.rBuf.Len = I2C_TERNIX_FLASH_CRC_MSG_LEN;
//    buff.rBuf.Data = rData;

//    result = I2CTernixRunSingleSessionTask(&buff);

//    return result;
//}

//I2CTask::ErrorCode I2CTask::I2CTernixBootLoaderReadVersion(guchar i2cAddress, guchar *rData)
//{
//    ErrorCode result = I2CTask::Success;
//    I2CMsgBuff buff = I2CTask::INITAILIZED_BUFF;

//    buff.i2cAddress = i2cAddress;
//    buff.cmd = I2C_TERNIX_CMD_BOOTLOADER_VERSION_READ;
//    buff.rBuf.Len = I2C_TERNIX_FLASH_VERSION_MSG_LEN;
//    buff.rBuf.Data = rData;

//    result = I2CTernixRunSingleSessionTask(&buff);

//    return result;
//}

//I2CTask::ErrorCode I2CTask::I2CTernixBootLoaderSendComposedFlashMsg(guchar i2cAddress, guchar *tData)
//{
//    ErrorCode result = I2CTask::Success;
//    I2CMsgBuff buff = I2CTask::INITAILIZED_BUFF;

//    buff.i2cAddress = i2cAddress;
//    buff.cmd = I2C_TERNIX_CMD_BOOTLOADER_FLASH_MSG_WRITE;
//    buff.tBuf.Len = I2C_TERNIX_FLASH_COMPOSED_MSG_LEN;
//    buff.tBuf.Data = tData;

//    result = I2CTernixRunSingleSessionTask(&buff);

//    return result;
//}





