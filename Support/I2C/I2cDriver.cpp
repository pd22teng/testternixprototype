#include "I2cDriver.h"
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <stropts.h>
#include <iostream>
#include <ostream>
#include <math.h>
#include <string>

extern "C" {
    #include "i2c-dev.h"
}

#define MAX_BUS 64

I2CDriver::I2CDriver(int i2cBus)
{
    this->I2CBus            = i2cBus;
    this->I2CBusFile        = 0;
    this->I2CDevice         = 0;

    open_i2c_bus(this->I2CBus);
}

I2CDriver::~I2CDriver()
{
    close_i2c_bus();
}

int I2CDriver::open_i2c_bus(int bus)
{
    int file;
    char namebuf[MAX_BUS];

    snprintf(namebuf, sizeof(namebuf), "/dev/i2c-%d", bus);

    if ((file = open(namebuf, O_RDWR)) < 0) {
        std::cerr << "I2CDRIVER: Failed to open I2C Bus" << bus << std::endl;
        return -1;
    }

    this->I2CBus = bus;
    this->I2CBusFile = file;

    return 1;
}

int I2CDriver::close_i2c_bus()
{
    if(this->I2CBusFile == 0) {
        std::cerr << "I2CDRIVER: Failed to close I2C bus, no I2C bus is opened." << std::endl;
        return -1;
    }

    if(close(this->I2CBusFile) < 0) {
        std::cerr << "I2CDRIVER: Failed to close I2C-" << this->I2CBus << std::endl;
        return -1;
    }

    this->I2CBus = 0;
    this->I2CDevice = 0;
    this->I2CBusFile = 0;

    return 1;
}

int I2CDriver::open_i2c_device(int address)
{
    if(this->I2CBusFile == 0) {
        std::cerr << "I2CDRIVER: Failed to open I2C device: " << address << " as no I2C bus opened." << std::endl;
        return -1;
    }

    if(this->I2CDevice != address) {
        if(ioctl(this->I2CBusFile, I2C_SLAVE, address) < 0) {
            std::cerr << "I2CDRIVER: Failed to open I2C device: " << address << std::endl;
            return -1;
        }
        this->I2CDevice = address;
    }
    return 1;
}

int I2CDriver::i2c_msg_transceiver(I2CMsgBuff *msg)
{
    int result = 1;

    if(open_i2c_device(msg->i2cAddress) < 0)
        return -1;

    if(msg->cmd == 0) {
        if(msg->rBuf.Len == 0)
            result = i2c_smbus_write_quick(this->I2CBusFile, I2C_SMBUS_WRITE);
        else if(msg->rBuf.Len == 1) {
            result = i2c_smbus_read_byte(this->I2CBusFile);

            if(result >= 0)
                msg->rBuf.Data[0] = (u_int8_t)result;
        }
        else
            result = -1;

        return result;
    }

    switch(msg->tBuf.Len) {
        case 0:
            switch(msg->rBuf.Len) {
                case 0: // write byte
                    result = i2c_smbus_write_byte(this->I2CBusFile, msg->cmd);
                    break;
                case 1: // read byte
                    result = i2c_smbus_read_byte_data(this->I2CBusFile, msg->cmd);
                    if(result >= 0)
                        msg->rBuf.Data[0] = (u_int8_t)result;
                    break;
                case 2: // read word
                    result = i2c_smbus_read_word_data(this->I2CBusFile, msg->cmd);
                    if(result >= 0) {
                        msg->rBuf.Data[0] = (u_int8_t)(result & 0xff);
                        msg->rBuf.Data[1] = (u_int8_t)(result >> 8);
                    }
                    break;
                default: // read block
                    result = i2c_smbus_read_i2c_block_data(this->I2CBusFile, msg->cmd, msg->rBuf.Len, msg->rBuf.Data);
                    if(result != msg->rBuf.Len)
                        result = -1;
                    break;
            }
            break;
        case 1:
            if(msg->rBuf.Len != 0) {
                result = i2c_smbus_block_process_call(this->I2CBusFile, msg->cmd, msg->tBuf.Len, msg->tBuf.Data);
//                std::cerr << "I2CDRIVER: Read analog sensor Data result: " << result << std::endl;

                if(result == msg->rBuf.Len) {
                    for(int i = 0; i < result; i++)
                        msg->rBuf.Data[i] = msg->tBuf.Data[i];
                }
            } else
                result = i2c_smbus_write_byte_data(this->I2CBusFile, msg->cmd, msg->tBuf.Data[0]);
            break;
        case 2:
            result = i2c_smbus_write_word_data(this->I2CBusFile, msg->cmd, (msg->tBuf.Data[0]|(msg->tBuf.Data[1] << 8)));
            break;
        default:
            result = i2c_smbus_write_i2c_block_data(this->I2CBusFile, msg->cmd, msg->tBuf.Len, msg->tBuf.Data);
            break;
    }

    return result;
}

int I2CDriver::i2c_write_data(const char *buff, int len)
{
    if(this->I2CDevice == 0) {
        std::cerr << "I2CDRIVER: Failed to write i2c data as no I2C device is opened." << std::endl;
        return -1;
    }

    if(this->I2CBusFile == 0) {
        std::cerr << "I2CDRIVER: Failed to write i2c data as no I2C bus is opened." << std::endl;
        return -1;
    }

    if(write(this->I2CBusFile, buff, len) != len) {
        std::cerr << "I2CDRIVER: Failed to write data to slave: " << this->I2CDevice << " through I2C-" << this->I2CBus << std::endl;
        return -1;
    }

    return 1;
}

int I2CDriver::i2c_read_data(char *buff, int len)
{
    if(this->I2CDevice == 0) {
        std::cerr << "I2CDRIVER: Failed to read i2c data as no I2C device is opened." << std::endl;
        return -1;
    }

    if(this->I2CBusFile == 0) {
        std::cerr << "I2CDRIVER: Failed to read i2c data as no I2C bus is opened." << std::endl;
        return -1;
    }

    if (read(this->I2CBusFile, buff, len) != len){
        std::cerr << "I2CDRIVER: Failed to read data from slave: " << this->I2CDevice << " through I2C-" << this->I2CBus << std::endl;
        return -1;
    }

    return 1;
}
