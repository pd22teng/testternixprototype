#ifndef I2CTASK_H
#define I2CTASK_H

#include "TernixI2CMessage.h"
#include "I2cDriver.h"

class I2CTask
{
public:
    explicit I2CTask(int i2cBus);
    ~I2CTask();

    enum ErrorCode {
           Success = 0, I2CBusOpenFail, I2CDeviceOpenFail, DataTransferFail, Other = 0xFF
    };

private:
    ErrorCode i2cRunSingleSessionTask(I2CMsgBuff *buff);
    void i2cNewSessionMessageInit(I2CMsgBuff * i2cMsg, guchar i2cAddress, guchar cmd, guchar tLen, guchar rLen, guchar* tBuff, guchar* rBuff);
public:
    ErrorCode i2cStartNewSession(guchar i2cAddress, guchar cmd, guchar tLen, guchar rLen, guchar* tBuff = 0, guchar* rBuff = 0);

    ErrorCode i2cIsDeviceReadyForSession(guchar i2cAddress);
private:
    I2CDriver *drv;

    const static I2CMsgBuff INITAILIZED_BUFF;
};

#endif // I2CTASK_H
