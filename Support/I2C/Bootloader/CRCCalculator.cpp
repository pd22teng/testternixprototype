#include "CRCCalculator.h"

CRCCalculator::CRCCalculator()
{
    initCRC();
}

void CRCCalculator::initCRC()
{
    crc = 0xffff;
    crc_polynomial = 0x1021;
    crc_calculated = false;
}

void CRCCalculator::updateCRC(unsigned char ch)
{
    unsigned char i = 0;
    unsigned char v = 0;
    unsigned char xor_flag = 0;

    v = 0x80;

    for(i = 0; i < 8; i++) {
      xor_flag = (crc & 0x8000) ? 1 : 0;
      crc <<= 1;

      if((ch & v) != 0)
        crc++;

      if(xor_flag != 0)
        crc ^= crc_polynomial;

      v >>= 1;
    }
}

unsigned short CRCCalculator::finishCRC()
{
    unsigned char i = 0;
    unsigned char xor_flag = 0;

    for(i = 0; i < 16; i++) {
      xor_flag = (crc & 0x8000) ? 1 : 0;

      crc <<= 1;

      if(xor_flag != 0)
        crc ^= crc_polynomial;
    }

    crc_calculated = true;

    return crc;
}

unsigned short CRCCalculator::getCRC()
{
    if(!crc_calculated)
        return 0xffff;
    else
        return crc;
}

unsigned short CRCCalculator::calculateCRC(const char *data, int size, int start, int end)
{
    unsigned char ch = 0;

    if((data != 0) && (size > 0) && (start >= 0) && (end >= 0) && (start < size) && (end < size) && (start <= end)) {
        initCRC();

        for(int i = start; i < end; i++) {
        	if((i + 1) % 4 == 0)
				ch = 0;
        	else
        		ch = (unsigned char)(data[i] & 0xff);
            updateCRC(ch);
        }

        finishCRC();

        return getCRC();
    }

    return (unsigned short)0xffff;
}
