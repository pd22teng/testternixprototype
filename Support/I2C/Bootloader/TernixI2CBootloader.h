#ifndef TERNIXI2CBOOTLOADER_H
#define TERNIXI2CBOOTLOADER_H

#include "I2cTask.h"
#include "HexParser.h"

class TernixI2CBootLoader
{
public:
    explicit TernixI2CBootLoader(I2CTask * i2cTask, HexParser * parser);
    ~TernixI2CBootLoader();

    I2CTask::ErrorCode sendWaitCmd(guchar i2cAddress);
    I2CTask::ErrorCode sendResetCmd(guchar i2cAddress);
    I2CTask::ErrorCode sendCRCCalculateCmd(guchar i2cAddress);

    I2CTask::ErrorCode isDeviceReadyForNextCMD(guchar i2cAddress);

    I2CTask::ErrorCode readCRC(guchar i2cAddress, guchar *rData);
    I2CTask::ErrorCode readVersion(guchar i2cAddress, guchar *rData);

    I2CTask::ErrorCode sendComposedFlashMsg(guchar i2cAddress, guchar *tData);

    I2CTask * getI2cTask();
    void setHexParser(HexParser * parser);

    void bootloaderRun(guchar i2cAddress);

private:
    I2CTask * i2cTask;
    HexParser * hexFile;

};

#endif // TERNIXI2CBOOTLOADER_H
