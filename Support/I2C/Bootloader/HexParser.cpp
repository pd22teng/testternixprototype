#include "HexParser.h"
#include "CRCCalculator.h"
#include <cstdlib>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string>
#include <iostream>
#include <fstream>

#include <boost/iostreams/filtering_stream.hpp>
#include <boost/iostreams/filtering_streambuf.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/progress.hpp>

using boost::timer;
using namespace std;

HexParser::HexParser()
{
    memoryImage = NULL;
    memoryImageReady = false;
    memoryImageBeginAddress = 0;
    memoryImageEndAddress   = 0;
    cCRC = 0xffff;
    progressRange = 0;
    progressPercent = 0;

    dataPackets = NULL;

    previousCompareIndex = 0;
}

HexParser::~HexParser()
{
    if(memoryImage != NULL) {
        delete memoryImage;
        memoryImage = NULL;
    }

    if(dataPackets != NULL) {
    	dataPackets->clear();
    	dataPackets = NULL;
    }
}

int HexParser::getDataPacketIndexByAddress(unsigned int address)
{
    uint i = 0;

    for(i = 0; i < dataPackets->size(); i++) {
        if(dataPackets->at(i).address == address)
            return i;
    }

    return -1;
}

int HexParser::compareDataPacket(const DataPacket & packet)
{
	uint i = 0;
    int j = 0;

    if((previousCompareIndex + 1) < dataPackets->size()) {
        if(dataPackets->at(previousCompareIndex + 1).address == packet.address)
            i = previousCompareIndex + 1;
        else
            i = 0;
    }
    else {
        i = 0;
    }

    for(; i < dataPackets->size(); i++) {
        if(dataPackets->at(i).address == packet.address) {
            for(j = 0; j < 8; j++) {
                if(dataPackets->at(i).buffer[j] != packet.buffer[j])
                    return 0;

                previousCompareIndex = i;
                return 1;
            }
        }
    }

    return 0;
}

int HexParser::setHexFileName(const string &hexFileName)
{
    if(this->hexFileName.length() == 0 || this->hexFileName.compare(hexFileName) != 0) {
        this->hexFileName = hexFileName;
        return 1;
    }
    cout << "The same hex file selected." << endl;

    return -1;
}

std::string HexParser::getHexFileName()
{
	return this->hexFileName;
}

void HexParser::readHexFile()
{
    unsigned char sum = 0;
    uint i = 0;
    int byte_count = 0;
    int bc = 0;
    uint address_calc = 0;
    uint first_address = 0;
    uint end_address = 0;
    uint addr_lo = 0;
    uint addr_hi = 0;
    int type = 0;
    int checksum = 0;
    unsigned char warning_flag = 0;

    if(this->hexFileName.length() == 0) {
    	cout << "Empty file name." << endl;
    	return;
    }

    memoryImageReady = false;
    memoryImageBeginAddress = 0;
    memoryImageEndAddress   = 0;

    if(memoryImage == NULL) {
        memoryImage = new array<char, HEX_MEMORY_BUFF_SIZE>();
		memoryImage->fill(0xff);
    }
    else
		memoryImage->fill(0xff);

    if(dataPackets == NULL) {
        dataPackets = new vector<DataPacket>;
    }
    else {
        dataPackets->clear();
    }

	timer time;
    ifstream file(this->hexFileName, ios_base::in);
    cout << "Hex file parsing start, please wait..." << endl;

	boost::iostreams::filtering_istream in;
	in.push(file);

	first_address = memoryImage->size() - 1;
	end_address = 0;

	for(string record; getline(in, record);) {
		if(record.at(0) == ':') {
			if(record.length() < 11) {
				cout << "Hex record too short" << endl;
				warning_flag = 1;
			}

			byte_count = parseHex8bit(record, 1);
			addr_lo    = parseHex16bit(record, 3);
			type       = parseHex8bit(record, 7);
			checksum   = parseHex8bit(record, record.length() - 3); // count the end of string '\0'

			sum = 0;
			for(i = 1; i < record.length() - 4; i += 2) {
				sum += parseHex8bit(record, i);
			}
			sum ^= 0xff;
			sum++;

			if(sum != checksum) {
				cout << "HEX record checksum does not match! 0x" << hex << sum << ", 0x" << hex << checksum << endl;
			}

			address_calc = addr_lo + addr_hi;
			bc = byte_count;

			for(i = 9; (i < record.length() - 4) && (bc != 0); i += 2) {
				bc--;

				if(type == 0) { // data
					if(address_calc < memoryImage->size()) {
						memoryImage->at(address_calc) = (char)parseHex8bit(record, i);
						if(address_calc > end_address)
							end_address = address_calc;

						if(address_calc < first_address)
							first_address = address_calc;
					}
					else {
						if((address_calc & 0xfffff0) != 0x300000) {
							cout << "Record does not fit in memory image. Address: 0x" << hex << address_calc << endl;
							warning_flag = 1;
						}
					}
				}

				address_calc++;
			}

			if(type == 0x00) {
				// hexParserLog(string("Data record"));
			}
			else if(type == 0x01) {
				// hexParserLog(string("Termination record"));
			}
			else if(type == 0x02) {
				// hexParserLog(string("Segment base address record"));
				addr_hi = parseHex16bit(record, 9);
				addr_hi <<= 4;
				// hexParserLog(string("Extended segment address: %1").arg(addr_hi, 0, 16));
			}
			else if(type == 0x04) {
				// hexParserLog(string("Extended linear address record"));
				addr_hi = parseHex16bit(record, 9);
				addr_hi <<= 16;
				// hexParserLog(string("Extended linear address: %1").arg(addr_hi, 0, 16));
			}
		}
	}

	file.close();

    if(warning_flag == 0) {
         cout << "File parsing is done, HEX file is OK!, " << "Start address: 0x" << hex << (int)first_address << ", End address: 0x" << (int)end_address << oct << ", "<< time.elapsed() << "s elapsed."<< endl;
    }
    else {
    	cout << "Warning: Problems reading hex file, " << time.elapsed() << "s elapsed." << endl;
    }

    first_address = 0x8000;
    setProgressFullRange(end_address - first_address);

    cout << "Hex file data extract start, please wait..." << endl;
    time.restart();

    for(uint addressCounter = first_address; addressCounter < end_address; addressCounter += 8) {
        DataPacket packet;

        packet.address = addressCounter/2;

        for(i = 0; i < 8; i++) {
            uint address = addressCounter + i;

            if(address < memoryImage->size() && address <= end_address)
                packet.buffer[i] = memoryImage->at(addressCounter + i);
            else
                packet.buffer[i] = 0xff;
        }

//        cout << "Datapacket: 0x" << hex << packet.address << " " << (int)packet.buffer[0] << " " << (int)packet.buffer[1] << " " << (int)packet.buffer[2] << " " << (int)packet.buffer[3]<< " " << (int)packet.buffer[4]<< " " << (int)packet.buffer[5]<< " " << (int)packet.buffer[6]<< " " << (int)packet.buffer[7] << endl;
        dataPackets->push_back(packet);
        setProgressPercent(addressCounter - first_address);
    }

    cout << "Data extraction done, Datapacket count: 0x" << hex <<dataPackets->size() << ", " << time.elapsed() << "s elapsed." << endl;

    memoryImageReady        = true;
    memoryImageBeginAddress = 0x4000*2;
    memoryImageEndAddress   = 0x2A7FC*2;

    calculateCRC();
    cout << "calculated CRC: 0x" << hex << cCRC << endl;
}

int HexParser::parseHex8bit(const string &record, int offset)
{
    string tmp = record.substr(offset, 2);

    int value = stoi(tmp, 0, 16);

	return value & 0xff;
}

int HexParser::parseHex16bit(const string &record, int offset)
{
    string tmp = record.substr(offset, 4);

    int value = stoi(tmp, 0, 16);

	return value & 0xffff;
}

int HexParser::beginAddress()
{
    return memoryImageBeginAddress/2;
}

int HexParser::endAddress()
{
    return memoryImageEndAddress/2;
}

unsigned short HexParser::getCRC()
{
	return cCRC;
}

void HexParser::calculateCRC()
{
    CRCCalculator crc;

    if(memoryImage == NULL)
    	cCRC = 0xffff;

    cCRC = crc.calculateCRC(memoryImage->data(), memoryImage->size(), memoryImageBeginAddress, memoryImageEndAddress);
}

int HexParser::dataPacketCount()
{
    if(dataPackets != NULL) {
        return dataPackets->size();
    }
    else {
        return 0;
    }
}

DataPacket HexParser::getDataPacket(uint packetNumber)
{
    DataPacket packet;

    packet.address = 0;

    for(int i = 0; i < 8; i++)
        packet.buffer[i] = 0xff;

    if(packetNumber < dataPackets->size()) {
        packet = dataPackets->at(packetNumber);
    }

    return packet;
}

bool HexParser::isHexImageReady()
{
	return memoryImageReady;
}

void HexParser::setProgressFullRange(int range)
{
	progressRange = range;
}

void HexParser::setProgressPercent(int percent)
{
	int pcent = 0;

	pcent = (percent * 100) / progressRange;

	if(progressPercent == pcent)
		return;

	progressPercent = pcent;

	if(progressPercent > 100)
		progressPercent = 100;

	std::stringstream out;
	pcent = (progressPercent/5);
	for(int i = 0 ; i < pcent; i++)
		out << "=";

	std::cout << "\r" << std::dec<< progressPercent << "% complete[" << out.str() << "]" << std::flush;
}




