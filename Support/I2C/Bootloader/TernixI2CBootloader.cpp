#include "TernixI2CBootloader.h"
#include <boost/progress.hpp>

using boost::timer;
using namespace std;

TernixI2CBootLoader::TernixI2CBootLoader(I2CTask * i2cTask, HexParser * parser)
{
    this->i2cTask = i2cTask;
    this->hexFile = parser;
}

TernixI2CBootLoader::~TernixI2CBootLoader()
{
	if(this->i2cTask != NULL) {
		delete this->i2cTask;
		this->i2cTask = NULL;
	}

	if(this->hexFile != NULL) {
		delete this->hexFile;
		this->hexFile = NULL;
	}
}

I2CTask::ErrorCode TernixI2CBootLoader::sendWaitCmd(guchar i2cAddress)
{
    return i2cTask->i2cStartNewSession(i2cAddress, I2C_TERNIX_CMD_BOOTLOADER_DATA_WAIT, I2C_TERNIX_NULL_MESSAGE_LEN, I2C_TERNIX_NULL_MESSAGE_LEN);
}

I2CTask::ErrorCode TernixI2CBootLoader::sendResetCmd(guchar i2cAddress)
{
    return i2cTask->i2cStartNewSession(i2cAddress, I2C_TERNIX_CMD_BOOTLOADER_RESET, I2C_TERNIX_NULL_MESSAGE_LEN, I2C_TERNIX_NULL_MESSAGE_LEN);
}

I2CTask::ErrorCode TernixI2CBootLoader::sendCRCCalculateCmd(guchar i2cAddress)
{
    return i2cTask->i2cStartNewSession(i2cAddress, I2C_TERNIX_CMD_BOOTLOADER_CRC_CALCULATE, I2C_TERNIX_NULL_MESSAGE_LEN, I2C_TERNIX_NULL_MESSAGE_LEN);
}

I2CTask::ErrorCode TernixI2CBootLoader::isDeviceReadyForNextCMD(guchar i2cAddress)
{
    return i2cTask->i2cStartNewSession(i2cAddress, I2C_TERNIX_CMD_BOOTLOADER_QUERY_STATE, I2C_TERNIX_NULL_MESSAGE_LEN, I2C_TERNIX_NULL_MESSAGE_LEN);
//    return i2cTask->i2cIsDeviceReadyForSession(i2cAddress);
}

I2CTask::ErrorCode TernixI2CBootLoader::readCRC(guchar i2cAddress, guchar *rData)
{
    return i2cTask->i2cStartNewSession(i2cAddress, I2C_TERNIX_CMD_BOOTLOADER_CRC_READ, I2C_TERNIX_NULL_MESSAGE_LEN, I2C_TERNIX_FLASH_CRC_MSG_LEN, NULL, rData);
}

I2CTask::ErrorCode TernixI2CBootLoader::readVersion(guchar i2cAddress, guchar *rData)
{
    return i2cTask->i2cStartNewSession(i2cAddress, I2C_TERNIX_CMD_BOOTLOADER_VERSION_READ, I2C_TERNIX_NULL_MESSAGE_LEN, I2C_TERNIX_FLASH_VERSION_MSG_LEN, NULL, rData);
}

I2CTask::ErrorCode TernixI2CBootLoader::sendComposedFlashMsg(guchar i2cAddress, guchar *tData)
{
    return i2cTask->i2cStartNewSession(i2cAddress, I2C_TERNIX_CMD_BOOTLOADER_FLASH_MSG_WRITE, I2C_TERNIX_FLASH_COMPOSED_MSG_LEN, I2C_TERNIX_NULL_MESSAGE_LEN, tData);
}

I2CTask * TernixI2CBootLoader::getI2cTask()
{
	return this->i2cTask;
}

void TernixI2CBootLoader::setHexParser(HexParser * parser)
{
	if(this->hexFile != NULL) {
		if(this->hexFile->getCRC() == parser->getCRC())
			return;

		delete this->hexFile;
	}
	this->hexFile = parser;
}

void TernixI2CBootLoader::bootloaderRun(guchar i2cAddress)
{
	timer time, time_query;
	DataPacket packet;
	int packetCounter = 0;
	unsigned char d[12];
	int i = 0;

	I2CTask::ErrorCode result = I2CTask::Success;

	if(!hexFile->isHexImageReady()) {
		cout << "Hex file is not ready for boot loading!" << endl;
		return;
	}

	cout << "Start boot loading, please wait..." << endl;

	if(sendResetCmd(i2cAddress) != I2CTask::Success) {
		cout << "Send reset Module CMD failed!" << endl;
		return;
	}

	cout << "Reset module" << endl;

	time.restart();

	while(time_query.elapsed() < 2) {
		result = isDeviceReadyForNextCMD(i2cAddress);
		if(result == I2CTask::DataTransferFail) continue;
		else break;
	}

	if(result != I2CTask::Success) {
		cout << "State-query fail, " << time_query.elapsed() << "s elapsed, " << result << "." <<endl;
		return;
	}

	time.restart();

	if(sendWaitCmd(i2cAddress) != I2CTask::Success) {
		cout << "Send wait flash data CMD failed!" << endl;
		return;
	}

	cout << "Send wait flash data CMD OK!" << endl;

	for(i = 0; i < 12; i++)
		d[i] = 0;

	hexFile->setProgressFullRange(hexFile->dataPacketCount());
	while(true) {
		if(packetCounter < hexFile->dataPacketCount()) {
			packet = hexFile->getDataPacket(packetCounter);

			d[0] = packet.address           & 0xff;
			d[1] = (packet.address >> 8)    & 0xff;
			d[2] = (packet.address >> 16)   & 0xff;
			d[3] = (packet.address >> 24)   & 0xff;

			for(i = 0; i < 8; i++)
				d[i + 4] = packet.buffer[i];

			time_query.restart();

			while(time_query.elapsed() < 3) {
				result = sendComposedFlashMsg(i2cAddress, &d[0]);

				if(result == I2CTask::DataTransferFail) continue;
				else break;
			}


			if(result != I2CTask::Success) {
				cout << "Send flash data failed, " << time_query.elapsed() << "s elapsed." << endl;
				break;
			}

			packetCounter++;
			hexFile->setProgressPercent(packetCounter);

//			cout << "Send HEX-data to analog module succeed, " << time_query.elapsed() << "s elapsed." << endl;
//			cout << "Address range: 0x" << hex << packet.address << " - 0x" << (packet.address+7) << endl;
//			cout << "HEX data: 0x" << hex << (int)d[0] << " 0x" << (int)d[1] << " 0x" << (int)d[2] << " 0x" << (int)d[3] << " 0x" << (int)d[4] << " 0x" << (int)d[5] << " 0x" << (int)d[6] << " 0x" << (int)d[7] << endl;

			time_query.restart();

			while(time_query.elapsed() < 5) {
				result = isDeviceReadyForNextCMD(i2cAddress);

				if(result == I2CTask::DataTransferFail) continue;
				else break;
			}

			if(result != I2CTask::Success) {
				cout << "State-query fail, " << time_query.elapsed() << "s elapsed, " << result << "." <<endl;
				break;
			}

//			cout << "Send state-query succeed, " << time_query.elapsed() << "s elapsed." << endl;
		} else {
//			int startAddress = hexFile->beginAddress();
//			int endAddress = hexFile->endAddress();
			unsigned short rCRC = 0xffff;
			unsigned short cCRC = 0xffff;

			cout << "Send CRC-query to analog module.";
//			cout << "Address range: " << hex << "0x" << startAddress << " - 0x" << endAddress << "." << endl;

			result = sendCRCCalculateCmd(i2cAddress);

			if(result != I2CTask::Success) {
				cout << " failed." << endl;
				break;
			}
			cout << " done." << endl;

			time_query.restart();

			while(time_query.elapsed() < 5) {
				result = isDeviceReadyForNextCMD(i2cAddress);

				if(result == I2CTask::DataTransferFail) continue;
				else break;
			}

			if(result != I2CTask::Success) {
				cout << "State-query fail." << endl;
				break;
			}

			result = readCRC(i2cAddress, &d[0]);

			if(result != I2CTask::Success) {
				cout << "CRC read failed." << endl;
				break;
			}

			rCRC = (unsigned short)(d[0] | d[1] << 8);
			cCRC = hexFile->getCRC();

			if(rCRC == cCRC)
				cout << "CRC check OK! cCRC: 0x" << hex << cCRC << ", rCRC: 0x" << hex << rCRC << endl;
			else
				cout << "CRC check FAILED! cCRC: 0x" << hex << cCRC << ", rCRC: 0x" << hex << rCRC << ", Firmware updated failed!" << endl;

			if(sendResetCmd(i2cAddress) != I2CTask::Success) {
				cout << "Send reset Module CMD failed!" << endl;
				break;
			}

			cout << "Reset module" << endl;

			break; // end
		}
	}

	if(result != I2CTask::Success)
		cout << "Boot loading failed," << time.elapsed() << "s elapsed." << endl;
	else
		cout << "Boot loading done," << time.elapsed() << "s elapsed." << endl;
}




