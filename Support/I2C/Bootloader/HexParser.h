#ifndef HEXPARSER_H
#define HEXPARSER_H

#include <string>
#include <vector>
#include <array>
#include <time.h>

#define HEX_MEMORY_BUFF_SIZE 0x80000 // 524288

typedef struct {
    unsigned int address;
    char buffer[8];
} DataPacket;

class HexParser
{

public:
    explicit HexParser();
    ~HexParser();

    void readHexFile(void);
    int getDataPacketIndexByAddress(unsigned int address);
    int compareDataPacket(const DataPacket & packet);
    int setHexFileName(const std::string & hexFileName);
    std::string getHexFileName();

    int dataPacketCount();
    DataPacket getDataPacket(uint packetNumber);

    int endAddress();
    int beginAddress();
    unsigned short getCRC();
    void setProgressFullRange(int range);
    void setProgressPercent(int percent);
    bool isHexImageReady();

private:
    int parseHex8bit(const std::string & record, int offset);
    int parseHex16bit(const std::string & record, int offset);
    void calculateCRC();

    std::array<char, HEX_MEMORY_BUFF_SIZE> *memoryImage;
    std::vector<DataPacket> *dataPackets;

    bool memoryImageReady;
    int memoryImageBeginAddress;
    int memoryImageEndAddress;
    unsigned short cCRC;
    uint previousCompareIndex;
    int progressRange;
    int progressPercent;
    std::string hexFileName;
};

#endif // HEXPARSER_H
