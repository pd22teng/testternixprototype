#ifndef CRCCALCULATOR_H
#define CRCCALCULATOR_H

class CRCCalculator
{
public:
    CRCCalculator();

    void initCRC();
    void updateCRC(unsigned char ch);
    unsigned short finishCRC();

    unsigned short calculateCRC(const char* data, int size, int start, int end);

    unsigned short getCRC();

private:
    unsigned short crc;
    unsigned short crc_polynomial;
    bool crc_calculated;
};

#endif // CRCCALCULATOR_H
