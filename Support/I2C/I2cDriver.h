#ifndef I2CDRIVER_H
#define I2CDRIVER_H

#include "../../Ternix.h"

#define I2C_BUS_1   1
#define I2C_BUS_2   2

typedef struct {
    guchar cmd;
    guchar i2cAddress;

    struct {
        guchar Len;
        guchar* Data;
    } tBuf;

    struct {
        guchar Len;
        guchar* Data;
    } rBuf;
} I2CMsgBuff;

class I2CDriver
{
public:
    explicit I2CDriver(int i2cBus);
    ~I2CDriver();

    int open_i2c_bus(int bus);
    int close_i2c_bus(void);
    int open_i2c_device(int address);

    int i2c_msg_transceiver(I2CMsgBuff* msg);

private:
    int i2c_write_data(const char* buff, int len);
    int i2c_read_data(char* buff, int len);

private:
    int  I2CBus;
    int  I2CDevice;
    int  I2CBusFile;
};

#endif // I2CDRIVER_H
