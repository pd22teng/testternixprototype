################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../Support/Daemon/TernixDaemonFunctions.cpp \
../Support/Daemon/TernixDaemonIOToolCMDParser.cpp 

OBJS += \
./Support/Daemon/TernixDaemonFunctions.o \
./Support/Daemon/TernixDaemonIOToolCMDParser.o 

CPP_DEPS += \
./Support/Daemon/TernixDaemonFunctions.d \
./Support/Daemon/TernixDaemonIOToolCMDParser.d 


# Each subdirectory must supply rules for building sources it contributes
Support/Daemon/%.o: ../Support/Daemon/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	arm-poky-linux-gnueabi-g++ -std=c++0x -I/opt/ternix/0.0.1/sysroots/cortexa5hf-vfp-poky-linux-gnueabi/usr/include/c++ -I"/home/teng/workspace/TernixSupport" -I/opt/ternix/0.0.1/sysroots/cortexa5hf-vfp-poky-linux-gnueabi/usr/lib/glib-2.0/include -I/opt/ternix/0.0.1/sysroots/cortexa5hf-vfp-poky-linux-gnueabi/usr/include/glib-2.0 -I/opt/ternix/0.0.1/sysroots/cortexa5hf-vfp-poky-linux-gnueabi/usr/include/boost -I"/home/teng/workspace/TestTernixPrototype" -I"/home/teng/workspace/TestTernixPrototype/Support" -I"/home/teng/workspace/TestTernixPrototype/IO" -I"/home/teng/workspace/TestTernixPrototype/Modules" -I"/home/teng/workspace/TestTernixPrototype/Support/GPIO" -I"/home/teng/workspace/TestTernixPrototype/Support/I2C" -I"/home/teng/workspace/TestTernixPrototype/Support/I2C/Bootloader" -I"/home/teng/workspace/TestTernixPrototype/Support/Daemon" -I"/home/teng/workspace/TestTernixPrototype/IO/Inputs" -I"/home/teng/workspace/TestTernixPrototype/IO/Outputs" -I"/home/teng/workspace/TestTernixPrototype/Modules/Analog" -I"/home/teng/workspace/TestTernixPrototype/Modules/Propo" -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


