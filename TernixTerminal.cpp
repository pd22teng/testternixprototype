#include "TernixTerminal.h"

TernixAnalogModule * TernixTerminal::analogList[TERNIX_ANALOG_MODULE_NUM];
TernixPropoModule * TernixTerminal::propoList[TERNIX_PROPO_MODULE_NUM];

TernixTerminal::TernixTerminal()
{
	char edge[7] = "rising";

    int i = 0;
    I2CTask * task  = NULL;

    task = new I2CTask(TERNIX_PROPO_MODULE_I2C_BUS);
    bootloader[TERNIX_PROPO_MODULE] = new TernixI2CBootLoader(task, NULL);

	task = new I2CTask(TERNIX_SENSOR_MODULE_I2C_BUS);
    bootloader[TERNIX_ANALOG_MODULE] = new TernixI2CBootLoader(task, NULL);

    // Prop modules
    for(i = TERNIX_PROPO_MODULE_1; i <= TERNIX_PROPO_MODULE_16; i++)
        propoList[i - 1] = new TernixPropoModule((guchar)i, bootloader[TERNIX_PROPO_MODULE]);

    // Analog modules
    for(i = TERNIX_ANALOG_MODULE_ONE; i <= TERNIX_ANALOG_MODULE_SIX; i++)
        analogList[i - 1] = new TernixAnalogModule((guchar)i, bootloader[TERNIX_ANALOG_MODULE]);

    if(gpio_set_input_mode(TernixAnalogModule::ternixAnalogModulesInterruptGPIO) >= 0) {
        gpio_set_glib_event_handler(TernixAnalogModule::ternixAnalogModulesInterruptGPIO, TernixAnalogModule::AnalogUpdateGpioEventHandler, (gpointer)NULL);

        gpio_set_edge(TernixAnalogModule::ternixAnalogModulesInterruptGPIO, edge);
    }
}

TernixTerminal::~TernixTerminal()
{
    int i = 0;

    // Delete Analog modules
    for(i = 0; i < TERNIX_ANALOG_MODULE_NUM; i++) {
        if(analogList[i] != NULL) {
            delete analogList[i];
            analogList[i] = NULL;
        }
    }

    // Delete Prop modules
    for(i = 0; i < TERNIX_PROPO_MODULE_NUM; i++) {
        if(propoList[i] != NULL) {
            delete propoList[i];
            propoList[i] = NULL;
        }
    }

    // Delete bootloader
    for(i = 0 ; i < 2; i++) {
    	if(bootloader[i] != NULL) {
    		delete bootloader[i];
    		bootloader[i] = NULL;
    	}
    }
}

void TernixTerminal::printOutAllPropoModulesConnectStatus()
{
	std::cerr << "Print out all propo modules connect status:" << std::endl;

    for(int i = 0; i < TERNIX_PROPO_MODULE_NUM; i++) {
        if(this->propoList[i]->isConnected())
            std::cerr << (int)this->propoList[i]->getId();
        else
            std::cerr << "--";

        if((i + 1) % 4 == 0)
            std::cerr << std::endl;
        else
            std::cerr << "\t";
    }
}

void TernixTerminal::printOutAllAnalogModulesConnectStatus()
{
	std::cerr << "Print out all analog modules connect status:" << std::endl;

    for(int i = 0; i < TERNIX_ANALOG_MODULE_NUM; i++) {
        if(this->analogList[i]->isConnected())
            std::cerr << (int)this->analogList[i]->getId();
        else
            std::cerr << "--";
        std::cerr << " ";
    }
    std::cerr << std::endl;
}

void TernixTerminal::printoutAllOutputsConnectStatus()
{
    std::cerr << "All connected output IDs:" << std::endl;

    for(int i = 0; i < TERNIX_PROPO_MODULE_NUM; i++) {
        if(this->propoList[i]->isConnected()) {
            this->propoList[i]->printOutAllChannelIDs();
        }
    }
}

void TernixTerminal::printoutAllInputsConnectStatus()
{
    std::cerr << "All connected input IDs:" << std::endl;

    for(int i = 0; i < TERNIX_ANALOG_MODULE_NUM; i++) {
        if(this->analogList[i]->isConnected()) {
            this->analogList[i]->printOutAllChannelIDs();
        }
    }
}

TernixI2CBootLoader * TernixTerminal::getI2cBootloader(gint id)
{
	if(id != TERNIX_PROPO_MODULE && id != TERNIX_ANALOG_MODULE)
		return NULL;

	return this->bootloader[id];
}

TernixPropoModule* TernixTerminal::getPropoModule(guchar moduleID)
{
	if(moduleID < TERNIX_PROPO_MODULE_1 || moduleID > TERNIX_PROPO_MODULE_16)
		return NULL;

	return propoList[moduleID - 1];
}

TernixAnalogModule* TernixTerminal::getAnalogModule(guchar moduleID)
{
	if(moduleID < TERNIX_ANALOG_MODULE_ONE || moduleID > TERNIX_ANALOG_MODULE_SIX)
		return NULL;

	return analogList[moduleID - 1];
}
