#ifndef TERNIXTERMINAL_H
#define TERNIXTERMINAL_H

#include "Ternix.h"
#include "TernixAnalogModule.h"
#include "TernixPropoModule.h"
#include "TernixI2CBootloader.h"

class TernixTerminal
{
public:
    TernixTerminal();
    ~TernixTerminal();

    void printOutAllPropoModulesConnectStatus();
    void printOutAllAnalogModulesConnectStatus();
    void printoutAllOutputsConnectStatus();
    void printoutAllInputsConnectStatus();
    TernixI2CBootLoader * getI2cBootloader(gint id);

    static TernixPropoModule * getPropoModule(guchar moduleID);
    static TernixAnalogModule * getAnalogModule(guchar moduleID);

private:
    TernixI2CBootLoader * bootloader[2];

    static TernixAnalogModule * analogList[TERNIX_ANALOG_MODULE_NUM];
    static TernixPropoModule * propoList[TERNIX_PROPO_MODULE_NUM];
};
#endif // TERNIXTERMINAL_H
