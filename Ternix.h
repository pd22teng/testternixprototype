#ifndef TERNIX_H
#define TERNIX_H

#include <Support/GPIO/GPIOFunctions.h>
#include <iostream>
/***
 * Ternix system Informations
 *
 */

#define OFF  0
#define ON   1

#define NO    0
#define YES   1

#define DISABLE  0
#define ENABLE   1

#define DIGITAL  0
#define ANALOG   1

#define INPUT   0
#define OUTPUT  1

#define TERNIX_PROPO_MODULE 0
#define TERNIX_ANALOG_MODULE 1

#define TERNIX_INPUT_NUM    32
#define TERNIX_OUTPUT_NUM   32

#endif // TERNIX_H
