#ifndef TERNIXINPUTDATATYPES_H
#define TERNIXINPUTDATATYPES_H

#include "Ternix.h"
#include "Support/GPIO/TernixPinOut.h"

#define TERNIX_INPUT_NULL   0
#define TERNIX_INPUT_ID_1   1
#define TERNIX_INPUT_ID_2   2
#define TERNIX_INPUT_ID_3   3
#define TERNIX_INPUT_ID_4   4
#define TERNIX_INPUT_ID_5   5
#define TERNIX_INPUT_ID_6   6
#define TERNIX_INPUT_ID_7   7
#define TERNIX_INPUT_ID_8   8
#define TERNIX_INPUT_ID_9   9
#define TERNIX_INPUT_ID_10  10
#define TERNIX_INPUT_ID_11  11
#define TERNIX_INPUT_ID_12  12
#define TERNIX_INPUT_ID_13  13
#define TERNIX_INPUT_ID_14  14
#define TERNIX_INPUT_ID_15  15
#define TERNIX_INPUT_ID_16  16
#define TERNIX_INPUT_ID_17  17
#define TERNIX_INPUT_ID_18  18
#define TERNIX_INPUT_ID_19  19
#define TERNIX_INPUT_ID_20  20
#define TERNIX_INPUT_ID_21  21
#define TERNIX_INPUT_ID_22  22
#define TERNIX_INPUT_ID_23  23
#define TERNIX_INPUT_ID_24  24
#define TERNIX_INPUT_ID_25  25
#define TERNIX_INPUT_ID_26  26
#define TERNIX_INPUT_ID_27  27
#define TERNIX_INPUT_ID_28  28
#define TERNIX_INPUT_ID_29  29
#define TERNIX_INPUT_ID_30  30
#define TERNIX_INPUT_ID_31  31
#define TERNIX_INPUT_ID_32  32

#define TERNIX_DISABLE_INPUT 0
#define TERNIX_ENABLE_INPUT  1

#define TERNIX_INPUT_ENABLED    0x01

#define TERNIX_DIGITAL_INPUT  0
#define TERNIX_ANALOG_INPUT   1

#define TERNIX_SET_INPUT_ANALOG     0x02

#define TERNIX_CURRENT_LOOP_MODE  0
#define TERNIX_VOLTAGE_MODE       1

#define TERNIX_INPUT_DEF_THRESHOLD      0x5

#define TERNIX_SET_INPUT_VOLTAGE_MODE   0x04

#define TERNIX_INPUT_DEFAULT_THRESHOLD    0x78

typedef union {
    guchar val;
    struct {
        guchar enable           : 1;   // Enable / Disable
        guchar mode             : 1;   // Analog / Digital
        guchar c_or_v           : 1;   // Input single: Curent loop / Voltage
        guchar threshold        : 4;   // Thredhold value for triggering value change interrupt
        guchar reserved         : 1;
    } bits;
} TernixInputControlRegisterByte;

#endif // TERNIXINPUTDATATYPES_H
