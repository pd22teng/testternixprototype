#include "TernixInput.h"
#include "TernixTerminal.h"

TernixInput* TernixInput::inputs[TERNIX_INPUT_NUM];
GMutex TernixInput::inputsMutex;
GCond TernixInput::inputsCond;

TernixInput::TernixInput(guchar inputId, guchar enable, guchar mode, guchar moduleID, guchar analogChannelID, guchar c_or_v, guchar threshold)
    :TernixIO(inputId, INPUT, getTernixInputGPIO(inputId - 1), moduleID, analogChannelID, enable, mode, threshold)
{ 
    currentOrVoltage = c_or_v;
    isInputGpioSet = NO;

    TernixInput::inputs[inputId - 1] = this;

    this->setInputGpioWorkMode();

    if(this->createInputGpioChannelAndBindEventHandler() < 0)
        std::cerr << "Setup input " << inputId << " event handler failed." << std::endl;

    g_thread_new("Ternix input thread " + inputId, TernixInput::TernixInputConRegThreadFunc, (gpointer)this);
}

TernixInput::~TernixInput()
{
}

void TernixInput::setInputCurrentSensor()
{
    lockConRegMutex();
    if(currentOrVoltage != TERNIX_CURRENT_LOOP_MODE) {
        currentOrVoltage = TERNIX_CURRENT_LOOP_MODE;
        setConRegUpdateFlag(TRUE);
        emitConRegConditionSignal();
    }
    unlockConRegMutex();
}

void TernixInput::setInputVoltageSensor()
{
    lockConRegMutex();
    if(currentOrVoltage != TERNIX_VOLTAGE_MODE) {
        currentOrVoltage = TERNIX_VOLTAGE_MODE;
        setConRegUpdateFlag(TRUE);
        emitConRegConditionSignal();
    }
    unlockConRegMutex();
}

guchar TernixInput::getInputSensorType()
{
    guchar type = 0;

    lockConRegMutex();
    type = currentOrVoltage;
    unlockConRegMutex();

    return type;
}

guchar TernixInput::getChannelSetupMsg()
{
    TernixInputControlRegisterByte byte;

    byte.val = 0;

    byte.bits.enable = getEnable();
    byte.bits.mode = getMode();
    byte.bits.c_or_v = getInputSensorType();
    byte.bits.threshold = getThreshold();

    return byte.val;
}

TernixInput *TernixInput::getTernixInput(guchar inputId)
{
    TernixInput * pin = NULL;

    if(inputId < TERNIX_INPUT_ID_1 || inputId > TERNIX_INPUT_ID_32){
        std::cerr << "Input ID: " << (int)inputId << " is invalid!" << std::endl;
        return NULL;
    }

    g_mutex_lock(&inputsMutex);
    pin = TernixInput::inputs[inputId - 1];
    g_mutex_unlock(&inputsMutex);

    return pin;
}

void TernixInput::setInputGpioWorkMode()
{
    if(gpio_set_input_mode(getGpio()) < 0)
        isInputGpioSet = NO;
    else
        isInputGpioSet = YES;
}

gint TernixInput::createInputGpioChannelAndBindEventHandler()
{
    char edge[5] = "both";
    guint gpio = getGpio();

    if(isInputGpioSet == NO)
        return -1;

    if(gpio_set_glib_event_handler(gpio, TernixInput::InputGpioEventHandler, (gpointer)this) < 0)
        return -1;

    if(gpio_set_edge(gpio, edge) < 0)
        return -1;

    return 0;
}

void TernixInput::printOutAllDigitInputs()
{
	std::cerr << "Print out all digital inputs:" << std::endl;

    for(int i = TERNIX_INPUT_ID_1; i <= TERNIX_INPUT_NUM; i++) {
        if(TernixInput::getTernixInput(i)->getDigit())
            std::cerr << "ON";
        else
            std::cerr << "OFF";

        if(i % 8 == 0)
            std::cerr << std::endl;
        else
            std::cerr << "\t";
    }
}

void TernixInput::printOutAllInputsMode()
{
	std::cerr << "Print out all input mode:" << std::endl;

    for(int i = TERNIX_INPUT_ID_1; i <= TERNIX_INPUT_NUM; i++) {
        if(TernixInput::getTernixInput(i)->getMode())
            std::cerr << "AN";
        else
            std::cerr << "DI";

        if(i % 8 == 0)
            std::cerr << std::endl;
        else
            std::cerr << "\t";
    }
}

gpointer TernixInput::TernixInputConRegThreadFunc(gpointer data)
{
    TernixInput* in = (TernixInput*)data;
    TernixAnalogModule * analog = TernixTerminal::getAnalogModule(in->getModuleID());

    while(TRUE) {
        in->lockConRegMutex();
        while(in->getConRegUpdateFlag() == FALSE)
            in->waitForConRegCondition();
        in->unlockConRegMutex();

        std::cerr << "Input: " << (int)in->getId() << " update config ";
        if(analog->i2cUpdateChannelConfig(in->getChannelID()) == I2CTask::Success)
            std::cerr << "succeed.";
        else
            std::cerr << "Failed.";
        std::cerr << std::endl;

        in->setConRegUpdateFlag(FALSE);
        g_thread_yield();
    }
    return NULL;
}

gboolean TernixInput::InputGpioEventHandler(GIOChannel *channel, GIOCondition condition, gpointer user_data)
{
    gchar buf[2];
    gsize byte_read = 0;
    GError *error = 0;
    TernixInput * input = (TernixInput*)user_data;

    g_io_channel_seek_position(channel, 0, G_SEEK_SET, 0);
    g_io_channel_read_chars(channel, buf, 1, &byte_read, &error);
    guchar digit = OFF;

    if(buf[0] == '0')
        digit = OFF;
    else if(buf[0] == '1')
        digit = ON;
    else
        return TRUE;

    input->setDigit(digit);

    std::cerr << "Input: " << (int)input->getId() << " value change to " << buf[0] << std::endl;

    return TRUE;
}
