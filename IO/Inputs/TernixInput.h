#ifndef TERNIXINPUT_H
#define TERNIXINPUT_H

#include "../TernixIO.h"
#include "TernixInputDataTypes.h"

class TernixInput: public TernixIO
{
public:
    TernixInput(guchar inputId, guchar enable, guchar mode, guchar moduleID, guchar analogChannelID, guchar c_or_v = TERNIX_CURRENT_LOOP_MODE, guchar threshold = TERNIX_INPUT_DEF_THRESHOLD);
    ~TernixInput();

    void setInputCurrentSensor();
    void setInputVoltageSensor();
    guchar getInputSensorType();

    guchar getChannelSetupMsg();

    static TernixInput * getTernixInput(guchar inputId);
    static void printOutAllDigitInputs();
    static void printOutAllInputsMode();
    static gpointer TernixInputConRegThreadFunc(gpointer data);
private:

    guchar currentOrVoltage;
    gboolean isInputGpioSet;

    static GMutex inputsMutex;
    static GCond inputsCond;
    static TernixInput* inputs[TERNIX_INPUT_NUM];

    void setInputGpioWorkMode();
    gint createInputGpioChannelAndBindEventHandler();

    static gboolean InputGpioEventHandler(GIOChannel* channel, GIOCondition condition, gpointer user_data);
};

#endif // TERNIXINPUT_H
