#include "TernixIO.h"

TernixIO::TernixIO(guchar id, guchar type, guchar gpio, guchar moduleID, guchar channelID)
{
    initIOStateRegister(id, type, gpio, moduleID, channelID);
    initIOControlRegister();

    if(type == OUTPUT) initIOValueRegister(LOW);
    else if(type == INPUT) initIOValueRegister(ON);
}

TernixIO::TernixIO(guchar id, guchar type, guchar gpio, guchar moduleID, guchar channelID, guchar enable, guchar mode, guchar threshold)
{
    initIOStateRegister(id, type, gpio, moduleID, channelID);
    initIOControlRegister(enable, mode, threshold);

    if(type == OUTPUT) initIOValueRegister(LOW);
    else if(type == INPUT) initIOValueRegister(ON);
}

TernixIO::~TernixIO()
{

}

void TernixIO::initIOStateRegister(guchar id, guchar type, guchar gpio, guchar moduleID, guchar channelID)
{
    staReg.id = id;
    staReg.type = type;
    staReg.gpio = gpio;
    staReg.moduleID = moduleID;
    staReg.channelID = channelID;
    g_mutex_init(&(staReg.mutex));
}

void TernixIO::initIOControlRegister(guchar enable, guchar mode, guchar threshold)
{
    conReg.enable = enable;
    conReg.mode = mode;
    conReg.threshold = threshold;
    conReg.updated = NO;

    g_mutex_init(&(conReg.mutex));
    g_mutex_init(&(conReg.changeMutex));
    g_cond_init(&(conReg.cond));
}

void TernixIO::initIOValueRegister(guchar digit, guchar analogVal)
{
    valReg.digit = digit;
    valReg.analogVal= analogVal;
    valReg.updated = NO;

    g_mutex_init(&(valReg.mutex));
    g_mutex_init(&(valReg.changeMutex));
    g_cond_init(&(valReg.cond));
}

guchar TernixIO::getId()
{
    guchar id = 0;

    g_mutex_lock(&staReg.mutex);
    id = staReg.id;
    g_mutex_unlock(&staReg.mutex);

    return id;
}

void TernixIO::setId(const guchar &value)
{
    g_mutex_lock(&staReg.mutex);
    staReg.id = value;
    g_mutex_unlock(&staReg.mutex);
}

guchar TernixIO::getType()
{
    guchar type = 0;

    g_mutex_lock(&staReg.mutex);
    type = staReg.type;
    g_mutex_unlock(&staReg.mutex);

    return type;
}

void TernixIO::setType(const guchar &value)
{
    g_mutex_lock(&staReg.mutex);
    staReg.type = value;
    g_mutex_unlock(&staReg.mutex);
}

guchar TernixIO::getGpio()
{
    guchar gpio = 0;

    g_mutex_lock(&staReg.mutex);
    gpio = staReg.gpio;
    g_mutex_unlock(&staReg.mutex);

    return gpio;
}

void TernixIO::setGpio(const guchar &value)
{
    g_mutex_lock(&staReg.mutex);
    staReg.gpio = value;
    g_mutex_unlock(&staReg.mutex);
}

guchar TernixIO::getChannelID()
{
    guchar chID = 0;

    g_mutex_lock(&staReg.mutex);
    chID = staReg.channelID;
    g_mutex_unlock(&staReg.mutex);

    return chID;
}

void TernixIO::setChannelID(const guchar &value)
{
    g_mutex_lock(&staReg.mutex);
    staReg.channelID = value;
    g_mutex_unlock(&staReg.mutex);
}

guchar TernixIO::getModuleID()
{
    guchar moID = 0;

    g_mutex_lock(&staReg.mutex);
    moID = staReg.moduleID;
    g_mutex_unlock(&staReg.mutex);

    return moID;
}

void TernixIO::setModuleID(const guchar &value)
{
    g_mutex_lock(&staReg.mutex);
    staReg.moduleID = value;
    g_mutex_unlock(&staReg.mutex);
}

guchar TernixIO::getEnable()
{
    guchar en = 0;

    g_mutex_lock(&conReg.mutex);
    en = conReg.enable;
    g_mutex_unlock(&conReg.mutex);

    return en;
}

void TernixIO::setEnable(const guchar &value)
{
    g_mutex_lock(&conReg.mutex);
    if(conReg.enable != value) {
        conReg.enable = value;
        setConRegUpdateFlag(TRUE);
        g_cond_signal(&conReg.cond);
    }
    g_mutex_unlock(&conReg.mutex);
}

guchar TernixIO::getMode()
{
    guchar md = 0;

    g_mutex_lock(&conReg.mutex);
    md = conReg.mode;
    g_mutex_unlock(&conReg.mutex);

    return md;
}

void TernixIO::setMode(const guchar &value)
{
    g_mutex_lock(&conReg.mutex);
    if(conReg.mode != value) {
        conReg.mode = value;
        setConRegUpdateFlag(TRUE);
        g_cond_signal(&conReg.cond);
    }
    g_mutex_unlock(&conReg.mutex);
}

guchar TernixIO::getThreshold()
{
    guchar thd = 0;

    g_mutex_lock(&conReg.mutex);
    thd = conReg.threshold;
    g_mutex_unlock(&conReg.mutex);

    return thd;
}

void TernixIO::setThreshold(const guchar &value)
{
    g_mutex_lock(&conReg.mutex);
    if(conReg.threshold != value) {
        conReg.threshold = value;
        setConRegUpdateFlag(TRUE);
        g_cond_signal(&conReg.cond);
    }
    g_mutex_unlock(&conReg.mutex);
}

gboolean TernixIO::getConRegUpdateFlag()
{
    guchar update = 0;

    g_mutex_lock(&conReg.changeMutex);
    update = conReg.updated;
    g_mutex_unlock(&conReg.changeMutex);

    return update;
}

void TernixIO::setConRegUpdateFlag(const gboolean &value)
{
    g_mutex_lock(&conReg.changeMutex);
    conReg.updated = value;
    g_mutex_unlock(&conReg.changeMutex);
}

void TernixIO::waitForConRegCondition()
{
    g_cond_wait(&conReg.cond, &conReg.mutex);
}

void TernixIO::lockConRegMutex()
{
    g_mutex_lock(&conReg.mutex);
}

void TernixIO::unlockConRegMutex()
{
    g_mutex_unlock(&conReg.mutex);
}

void TernixIO::emitConRegConditionSignal()
{
    g_cond_signal(&conReg.cond);
}

guint16 TernixIO::getAnalogVal()
{
    guint16 anVal = 0;

    g_mutex_lock(&valReg.mutex);
    anVal = valReg.analogVal;
    g_mutex_unlock(&valReg.mutex);

    return anVal;
}

void TernixIO::setAnalogVal(const guint16 &value)
{
    g_mutex_lock(&valReg.mutex);
    if(valReg.analogVal != value) {
        valReg.analogVal = value;
        setValRegUpdateFlag(TERNIX_IO_ANALOG_VAL_UPDATED);
        g_cond_signal(&valReg.cond);
    }
    g_mutex_unlock(&valReg.mutex);
}

guchar TernixIO::getDigit()
{
    guchar di = 0;

    g_mutex_lock(&valReg.mutex);
    di = valReg.digit;
    g_mutex_unlock(&valReg.mutex);

    return di;
}

void TernixIO::setDigit(const guchar &value)
{
    g_mutex_lock(&valReg.mutex);
    if(valReg.digit != value) {
        valReg.digit = value;
        setValRegUpdateFlag(TERNIX_IO_DIGIT_VAL_UPDATED);
        g_cond_signal(&valReg.cond);
    }
    g_mutex_unlock(&valReg.mutex);
}

guchar TernixIO::getValRegUpdateFlag()
{
    guchar update = 0;

    g_mutex_lock(&valReg.changeMutex);
    update = valReg.updated;
    g_mutex_unlock(&valReg.changeMutex);

    return update;
}

void TernixIO::setValRegUpdateFlag(const guchar &value)
{
    g_mutex_lock(&valReg.changeMutex);
    valReg.updated = value;
    g_mutex_unlock(&valReg.changeMutex);
}

void TernixIO::waitForValRegCondition()
{
    g_cond_wait(&valReg.cond, &valReg.mutex);
}

void TernixIO::lockValRegMutex()
{
    g_mutex_lock(&valReg.mutex);
}

void TernixIO::unlockValRegMutex()
{
    g_mutex_unlock(&valReg.mutex);
}

void TernixIO::emitValRegConditionSignal()
{
    g_cond_signal(&valReg.cond);
}

