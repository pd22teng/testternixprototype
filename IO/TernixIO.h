#ifndef TERNIXIO_H
#define TERNIXIO_H

#include "Ternix.h"

#define TERNIX_IO_DIGIT_VAL_UPDATED  1
#define TERNIX_IO_ANALOG_VAL_UPDATED 2

typedef struct {
    guchar id;
    guchar type;
    guchar gpio;
    guchar moduleID;
    guchar channelID;
    GMutex mutex;
} TernixIOStateRegister;

typedef struct {
    guchar enable;  // Enable/Disable
    guchar mode;    // Analog/Digital
    guchar threshold; // State change detect threshold
    gboolean updated;
    GMutex mutex;
    GMutex changeMutex;
    GCond  cond;
} TernixIOControlRegister;

typedef struct {
    guchar digit;
    guint16 analogVal;
    guchar updated;
    GMutex mutex;
    GMutex changeMutex;
    GCond  cond;
} TernixIOValueRegister;

class TernixIO
{
public:
    TernixIO(guchar id, guchar type, guchar gpio, guchar moduleID, guchar channelID);
    TernixIO(guchar id, guchar type, guchar gpio, guchar moduleID, guchar channelID, guchar enable, guchar mode, guchar threshold);

    virtual ~TernixIO();
    virtual guchar getChannelSetupMsg() = 0;

    guchar getId();
    void setId(const guchar &value);

    guchar getType();
    void setType(const guchar &value);

    guchar getGpio();
    void setGpio(const guchar &value);

    guchar getChannelID();
    void setChannelID(const guchar &value);

    guchar getModuleID();
    void setModuleID(const guchar &value);

    guchar getEnable();
    void setEnable(const guchar &value);

    guchar getMode();
    void setMode(const guchar &value);

    guchar getThreshold();
    void setThreshold(const guchar &value);

    gboolean getConRegUpdateFlag();
    void setConRegUpdateFlag(const gboolean &value);

    void waitForConRegCondition();
    void lockConRegMutex();
    void unlockConRegMutex();

    void emitConRegConditionSignal();

    guint16 getAnalogVal();
    void setAnalogVal(const guint16 &value);

    guchar getDigit();
    void setDigit(const guchar &value);

    guchar getValRegUpdateFlag();
    void setValRegUpdateFlag(const guchar &value);

    void waitForValRegCondition();
    void lockValRegMutex();
    void unlockValRegMutex();

    void emitValRegConditionSignal();
private:
    TernixIOStateRegister staReg;
    TernixIOControlRegister conReg;
    TernixIOValueRegister valReg;

    void initIOStateRegister(guchar id, guchar type, guchar gpio, guchar moduleID, guchar channelID);
    void initIOControlRegister(guchar enable = ENABLE, guchar mode = DIGITAL, guchar threshold = 0);
    void initIOValueRegister(guchar digit, guchar analogVal = 0);
};

#endif // TERNIXIO_H
