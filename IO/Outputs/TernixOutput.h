#ifndef TERNIXOUTPUT_H
#define TERNIXOUTPUT_H

#include "../TernixIO.h"
#include "TernixOutputDataTypes.h"

class TernixOutput: public TernixIO
{
public:
    TernixOutput(guchar outputId, guchar enable, guchar mode, guchar moduleId, guchar propoChannelID, guchar threshold);
    ~TernixOutput();

    void setPWMDC(guint16 dc);
    guint16 getPWMDC(void);

    void setPWMFreq(guchar freq);
    guchar getPWMFreq(void);

    guchar getChannelSetupMsg();

    static TernixOutput * getTernixOutput(guchar outputId);
    static void printOutAllDigitOutputs();
    static gpointer TernixOutputValRegThreadFunc(gpointer data);
    static gpointer TernixOutputConRegThreadFunc(gpointer data);
private:
    guchar pwmFreq;
    static GMutex outputsMutex;
    static GCond outputsCond;
    static TernixOutput* outputs[TERNIX_OUTPUT_NUM];
};

#endif // TERNIXOUTPUT_H
