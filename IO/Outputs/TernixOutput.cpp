#include "TernixOutput.h"
#include "TernixTerminal.h"

TernixOutput* TernixOutput::outputs[TERNIX_OUTPUT_NUM];

GMutex TernixOutput::outputsMutex;
GCond TernixOutput::outputsCond;

TernixOutput::TernixOutput(guchar outputId, guchar enable, guchar mode, guchar moduleId, guchar propoChannelID, guchar threshold)
    :TernixIO(outputId, OUTPUT, getTernixOutputGPIO(outputId - 1), moduleId, propoChannelID, enable, mode, threshold)
{
    pwmFreq = PWM_FREQ_DFT;

    TernixOutput::outputs[outputId - 1] = this;

    gpio_set_output_mode(getGpio());

    g_thread_new("Ternix output value thread " + outputId, TernixOutput::TernixOutputValRegThreadFunc, (gpointer)this);
    g_thread_new("Ternix output control thread " + outputId, TernixOutput::TernixOutputConRegThreadFunc, (gpointer)this);
}

TernixOutput::~TernixOutput()
{
}

void TernixOutput::setPWMDC(guint16 dc)
{
	guint16 duty_10bits = 0;

    if(dc <= 100) {

    	duty_10bits = dc * 10;

        setAnalogVal(duty_10bits);
    }
}

guint16 TernixOutput::getPWMDC()
{
    return getAnalogVal();
}

void TernixOutput::setPWMFreq(guchar freq)
{
    if(freq >= PWM_FREQ_MIN && freq <= PWM_FREQ_MAX) {
        lockValRegMutex();
            if(pwmFreq != freq) {
                pwmFreq = freq;
                setValRegUpdateFlag(TERNIX_OUTPUT_PWM_FREQ);
                emitValRegConditionSignal();
        }
        unlockValRegMutex();
    }
}

guchar TernixOutput::getPWMFreq()
{
    guchar freq = 0;

    lockValRegMutex();
    freq = pwmFreq;
    unlockValRegMutex();

    return freq;
}

guchar TernixOutput::getChannelSetupMsg()
{
    TernixOutputControlRegisterByte byte;

    byte.val = 0;

    byte.bits.enable = getEnable();
    byte.bits.mode = getMode();
    byte.bits.OC_threshold = getThreshold();

    return byte.val;
}

TernixOutput *TernixOutput::getTernixOutput(guchar outputId)
{
    TernixOutput * pout = NULL;

    if(outputId < TERNIX_OUTPUT_ID_1 || outputId > TERNIX_OUTPUT_ID_32) {
        std::cerr << "Output ID: " << (int)outputId << " is invalid!" << std::endl;
        return NULL;
    }

    g_mutex_lock(&outputsMutex);
    pout = TernixOutput::outputs[outputId - 1];
    g_mutex_unlock(&outputsMutex);

    return pout;
}

void TernixOutput::printOutAllDigitOutputs()
{
	std::cerr << "Print out all digital outputs:" << std::endl;

    for(int i = TERNIX_OUTPUT_ID_1; i <= TERNIX_OUTPUT_NUM; i++) {
        if(TernixOutput::getTernixOutput(i)->getDigit())
            std::cerr << "HI";
        else
            std::cerr << "LO";

        if(i % 8 == 0)
            std::cerr << std::endl;
        else
            std::cerr << "\t";
    }
}

gpointer TernixOutput::TernixOutputConRegThreadFunc(gpointer data)
{
    TernixOutput* out = (TernixOutput*)data;
    TernixPropoModule * propo = TernixTerminal::getPropoModule(out->getModuleID());

    while(TRUE) {
        out->lockConRegMutex();
        while(out->getConRegUpdateFlag() == FALSE)
            out->waitForConRegCondition();
        out->unlockConRegMutex();

        std::cerr << "Output: " << (int)out->getId() << " update config ";
        if(propo->i2cUpdateChannelConfig(out->getChannelID()) == I2CTask::Success)
            std::cerr << "succeed.";
        else
            std::cerr << "Failed.";
        std::cerr << std::endl;

        out->setConRegUpdateFlag(FALSE);
        g_thread_yield();
    }

    return NULL;
}

gpointer TernixOutput::TernixOutputValRegThreadFunc(gpointer data)
{
    TernixOutput* out = (TernixOutput*)data;
    TernixPropoModule * propo = TernixTerminal::getPropoModule(out->getModuleID());

    guint digit = 0;
    gint gpio = 0;
    guchar updateCode = 0;

    while(TRUE) {
        out->lockValRegMutex();
        while((updateCode = out->getValRegUpdateFlag()) == NO)
            out->waitForValRegCondition();
        out->unlockValRegMutex();

        switch(updateCode) {
            case TERNIX_OUTPUT_DIDGIT:
                gpio = out->getGpio();
                if(out->getDigit()) gpio_set_value(gpio, HIGH);
                else gpio_set_value(gpio, LOW);
                gpio_get_value(gpio, &digit);
                std::cerr << "Output: " << (int)out->getId() << ", value changed to: " << digit << "." << std::endl;
                break;
            case TERNIX_OUTPUT_PWM_DC:
                std::cerr << "Output: " << (int)out->getId() << " set Duty Cycle ";
                if(propo->TernixPropoModuleSetPWMDC((out->getChannelID() - 1), out->getPWMDC()) == I2CTask::Success)
                    std::cerr << "succeed.";
                else
                    std::cerr << "Failed.";
                std::cerr << std::endl;
                break;
            case TERNIX_OUTPUT_PWM_FREQ:
                std::cerr << "Output: " << (int)out->getId() << " set Frequency ";
                if(propo->TernixPropoModuleSetPWMFreq((out->getChannelID() - 1), out->getPWMFreq()) == I2CTask::Success)
                    std::cerr << "succeed.";
                else
                    std::cerr << "Failed.";
                std::cerr << std::endl;
                break;
            default: break;
        }
        out->setValRegUpdateFlag(NO);
        g_thread_yield();
    }

    return NULL;
}
