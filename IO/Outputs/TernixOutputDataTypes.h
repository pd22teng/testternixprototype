#ifndef TERNIXOUTPUTDATATYPES_H
#define TERNIXOUTPUTDATATYPES_H

#include "Ternix.h"
#include "Support/GPIO/TernixPinOut.h"

#define TERNIX_OUTPUT_NULL   0
#define TERNIX_OUTPUT_ID_1   1
#define TERNIX_OUTPUT_ID_2   2
#define TERNIX_OUTPUT_ID_3   3
#define TERNIX_OUTPUT_ID_4   4
#define TERNIX_OUTPUT_ID_5   5
#define TERNIX_OUTPUT_ID_6   6
#define TERNIX_OUTPUT_ID_7   7
#define TERNIX_OUTPUT_ID_8   8
#define TERNIX_OUTPUT_ID_9   9
#define TERNIX_OUTPUT_ID_10  10
#define TERNIX_OUTPUT_ID_11  11
#define TERNIX_OUTPUT_ID_12  12
#define TERNIX_OUTPUT_ID_13  13
#define TERNIX_OUTPUT_ID_14  14
#define TERNIX_OUTPUT_ID_15  15
#define TERNIX_OUTPUT_ID_16  16
#define TERNIX_OUTPUT_ID_17  17
#define TERNIX_OUTPUT_ID_18  18
#define TERNIX_OUTPUT_ID_19  19
#define TERNIX_OUTPUT_ID_20  20
#define TERNIX_OUTPUT_ID_21  21
#define TERNIX_OUTPUT_ID_22  22
#define TERNIX_OUTPUT_ID_23  23
#define TERNIX_OUTPUT_ID_24  24
#define TERNIX_OUTPUT_ID_25  25
#define TERNIX_OUTPUT_ID_26  26
#define TERNIX_OUTPUT_ID_27  27
#define TERNIX_OUTPUT_ID_28  28
#define TERNIX_OUTPUT_ID_29  29
#define TERNIX_OUTPUT_ID_30  30
#define TERNIX_OUTPUT_ID_31  31
#define TERNIX_OUTPUT_ID_32  32

#define TERNIX_DISABLE_OUTPUT 0
#define TERNIX_ENABLE_OUTPUT  1

#define TERNIX_OUTPUT_ENABLED   0x01

#define TERNIX_DIGIT_OUTPUT  0
#define TERNIX_PROPO_OUTPUT  1

#define TERNIX_SET_OUTPUT_PROPO 0x02

#define TERNIX_OUTPUT_DEF_OC_THRESHOLD      0x10

// Buff updated id
#define TERNIX_OUTPUT_DIDGIT    1
#define TERNIX_OUTPUT_PWM_DC    2
#define TERNIX_OUTPUT_PWM_FREQ  3

typedef enum {
    PWM_FREQ_MIN = 1,
    PWM_50HZ     = 1,
    PWM_100HZ    = 2,
    PWM_150HZ    = 3,
    PWM_200HZ    = 4,
    PWM_FREQ_DFT = 4,
    PWM_250HZ    = 5,
    PWM_300HZ    = 6,
    PWM_350HZ    = 7,
    PWM_400HZ    = 8,
    PWM_450HZ    = 9,
    PWM_500HZ    = 10,
    PWM_550HZ    = 11,
    PWM_600HZ    = 12,
    PWM_FREQ_MAX = 12
} TernixOutputPWMFreq;

typedef union {
    guchar val;
    struct {
        guchar enable           : 1; // Enable / Disable
        guchar mode             : 1; // Propo / Digital
        guchar OC_threshold     : 5; // threshold for over current
        guchar reserved         : 1;
    } bits;
} TernixOutputControlRegisterByte;

#endif // TERNIXOUTPUTDATATYPES_H
